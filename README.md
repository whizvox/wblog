# Wblog Repository

Nothing to see yet. Come back later.

## Gradle Commands

Run at your own risk.

* **`testsuite:run`**: Runs the test suite module through port 4568
by default. Go to `localhost:4568/debug/shutdown` to properly
shutdown the server.
* **`cleanupFiles`** and **`testsuite:cleanupFiles`**: Cleans up unneeded files
from the test suite.