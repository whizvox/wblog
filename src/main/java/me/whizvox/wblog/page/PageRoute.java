package me.whizvox.wblog.page;

import me.whizvox.wblog.Reference;
import me.whizvox.wblog.Wblog;
import me.whizvox.wblog.dbo.CachedPage;
import me.whizvox.wblog.dbo.Page;
import me.whizvox.wblog.dbo.User;
import spark.Request;
import spark.Response;
import spark.Route;

import java.util.HashMap;
import java.util.Map;

import static spark.Spark.halt;
import static me.whizvox.wblog.Wblog.LOGGER;

public class PageRoute implements Route {

  private Wblog wblog;

  public PageRoute(Wblog wblog) {
    this.wblog = wblog;
  }

  @Override
  public Object handle(Request request, Response response) throws Exception {
    String pagePath = request.params(Reference.ParamKeys.PAGE_NAME);
    if (pagePath == null || pagePath.isEmpty()) {
      // TODO: Make the landing page configurable
      pagePath = "home";
    }
    String lang = request.session().attribute(Reference.SessionKeys.LANGUAGE);
    User user = request.session().attribute(Reference.SessionKeys.USER);

    if (lang == null) {
      LOGGER.error("Could not retrieve session variable lang <%s, %s>", request.ip(), request.userAgent());
      halt(500, "Language not established. Please report this to an admin.");
    }

    CachedPage cachedPage = wblog.getPageService().getCachedPage(pagePath, lang);
    if (cachedPage == null) {
      Page page = wblog.getPageService().getPage(pagePath, lang);

      if (page == null) {
        halt(404);
      }

      HtmlPage htmlPage = PageHelper.setupHtmlPage();
      htmlPage.title = page.title;
      htmlPage.language = page.language;

      Map<String, Object> dm = new HashMap<>();
      dm.put("i18n", wblog.getLocalizer());
      dm.put("page", page);
      dm.put("htmlPage", htmlPage);

      LOGGER.info("Page <%s, %s> not found in the cache database. Creating a new entry...", pagePath, lang);
      cachedPage = wblog.getPageService().getRenderedPage(page, dm, false);
    }

    return cachedPage.contents;
  }

}
