package me.whizvox.wblog.page;

import lombok.Getter;

import java.util.List;

public class NavigablePage extends HtmlPage {

  @Getter
  public List<NavigationEntry> navbar;

}
