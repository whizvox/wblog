package me.whizvox.wblog.page;

import lombok.Getter;

import java.util.List;

public class NavigationEntry {

  @Getter
  public String href;

  @Getter
  public String title;

  @Getter
  public List<NavigationEntry> subEntries;

}
