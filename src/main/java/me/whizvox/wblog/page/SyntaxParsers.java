package me.whizvox.wblog.page;

import me.whizvox.wblog.util.HtmlUtil;
import me.whizvox.wblog.util.MarkdownUtils;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class SyntaxParsers {

  private SyntaxParsers() {}

  public static final SyntaxParser plain = HtmlUtil::sanitizePlainText;

  public static final SyntaxParser html = HtmlUtil::sanitizeHtml;

  public static final SyntaxParser markdown = contents -> MarkdownUtils.render(HtmlUtil.sanitizeHtml(contents));

  public static final Map<String, SyntaxParser> DEFAULT_PARSERS;

  static {
    Map<String, SyntaxParser> parsers = new HashMap<>();
    parsers.put("plain", plain);
    parsers.put("html", html);
    parsers.put("markdown", markdown);
    DEFAULT_PARSERS = Collections.unmodifiableMap(parsers);
  }

}
