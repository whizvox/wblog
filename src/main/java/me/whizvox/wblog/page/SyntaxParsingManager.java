package me.whizvox.wblog.page;

import java.util.HashMap;
import java.util.Map;

public class SyntaxParsingManager {

  private Map<String, SyntaxParser> parsers;
  private SyntaxParser fallbackParser;

  private SyntaxParsingManager(Map<String, SyntaxParser> parsers, SyntaxParser fallbackParser) {
    this.parsers = parsers;
    this.fallbackParser = fallbackParser;
  }

  public String parse(String syntaxName, String contents) {
    SyntaxParser parser = parsers.getOrDefault(syntaxName, null);
    if (parser != null) {
      return parser.parse(contents);
    }
    return fallbackParser.parse(contents);
  }

  public static Builder builder() {
    return new Builder();
  }

  public static class Builder {
    public Map<String, SyntaxParser> parsers;
    public SyntaxParser fallbackParser;

    public Builder() {
      parsers = new HashMap<>();
    }

    public Builder addParser(String name, SyntaxParser parser) {
      parsers.put(name, parser);
      return this;
    }

    public Builder setFallbackParser(SyntaxParser fallbackParser) {
      this.fallbackParser = fallbackParser;
      return this;
    }

    public Builder addDefaultParsers() {
      parsers.putAll(SyntaxParsers.DEFAULT_PARSERS);
      return this;
    }

    public SyntaxParsingManager build() {
      if (fallbackParser == null) {
        fallbackParser = SyntaxParsers.plain;
      }
      if (parsers.isEmpty()) {
        addDefaultParsers();
      }
      return new SyntaxParsingManager(parsers, fallbackParser);
    }

  }

}
