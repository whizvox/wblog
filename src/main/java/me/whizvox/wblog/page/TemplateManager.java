package me.whizvox.wblog.page;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.TemplateNotFoundException;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;

public class TemplateManager {

  private Configuration config;

  public TemplateManager(Configuration config) {
    this.config = config;
  }

  public Configuration getConfig() {
    return config;
  }

  public boolean process(Writer output, String name, Object dataModel) throws IOException, TemplateException {
    Template temp;
    try {
      temp = config.getTemplate(name + ".ftlh");
    } catch (TemplateNotFoundException ignored) {
      return false;
    }
    temp.process(dataModel, output);
    return true;
  }

  public String process(String name, Object dataModel) throws TemplateException {
    StringWriter writer = new StringWriter();
    try {
      if (process(writer, name, dataModel)) {
        return writer.toString();
      }
    } catch (IOException ignored) {}
    return null;
  }

}
