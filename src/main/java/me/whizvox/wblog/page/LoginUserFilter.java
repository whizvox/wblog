package me.whizvox.wblog.page;

import me.whizvox.wblog.Reference;
import me.whizvox.wblog.Wblog;
import me.whizvox.wblog.auth.AuthenticationService;
import me.whizvox.wblog.dbo.User;
import spark.Filter;
import spark.Request;
import spark.Response;

import java.nio.LongBuffer;

import static me.whizvox.wblog.Wblog.LOGGER;

public class LoginUserFilter implements Filter {

  private Wblog wblog;

  public LoginUserFilter(Wblog wblog) {
    this.wblog = wblog;
  }

  @Override
  public void handle(Request request, Response response) throws Exception {
    User user = request.session().attribute(Reference.SessionKeys.USER);
    if (user == null) {
      String tokenStr = request.cookies().getOrDefault(Reference.CookieKeys.LOGIN_TOKEN, null);
      if (tokenStr != null) {
        LongBuffer idBuffer = LongBuffer.allocate(1);
        AuthenticationService.VerifyLoginResult res = wblog.getAuthenticationService().verifyLogin(tokenStr, idBuffer);
        if (res != AuthenticationService.VerifyLoginResult.SUCCESS) {
          LOGGER.error("Client <%s, %s> has an invalid login token cookie <%s, %s>. Will now remove...", request.ip(),
              request.userAgent(), tokenStr, res);
          response.removeCookie(Reference.CookieKeys.LOGIN_TOKEN);
        }
        long userId = idBuffer.get();

        user = wblog.getAuthenticationService().getUserRepository().selectWithId(userId);
        if (user == null) {
          LOGGER.error("Client <%s, %s> has a login token <%s> that doesn't correlate to any existing users <%d>. " +
              "Will now remove...", request.ip(), request.userAgent(), tokenStr, userId);
          response.removeCookie(Reference.CookieKeys.LOGIN_TOKEN);
          wblog.getAuthenticationService().getLoginRepository().deleteWithToken(tokenStr);
        }
      }
      request.session().attribute(Reference.SessionKeys.USER, user);
    }
  }

}
