package me.whizvox.wblog.page;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import me.whizvox.wblog.dbo.Page;
import me.whizvox.wblog.util.StringUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.*;

public class NavigationManager {

  private PageService pageService;
  private Map<String, List<NavigationEntry>> navigation;

  public NavigationManager(PageService pageService) {
    this.pageService = pageService;
    this.navigation = new HashMap<>();
  }

  public Map<String, List<NavigationEntry>> getNavigation() {
    return navigation;
  }

  public List<NavigationEntry> getNavigation(String language) {
    List<NavigationEntry> nav = navigation.getOrDefault(language, null);
    if (nav == null) {
      nav = navigation.get(pageService.getLocalizer().getFallbackLanguage());
    }
    return nav;
  }

  private void processNavigationEntry(String language, NavigationEntry navEntry) {
    if (StringUtils.isNullOrEmpty(navEntry.href)) {
      navEntry.href = "#";
    }
    if (StringUtils.isNullOrEmpty(navEntry.title) && !navEntry.href.equals("#")) {
      Page page = pageService.getPage(language, navEntry.href);
      if (page != null) {
        navEntry.title = page.title;
      } else {
        navEntry.title = "NO_PAGE_" + navEntry.href;
      }
    } else {
      navEntry.title = "NOT_DEFINED";
    }
    if (!StringUtils.isNullOrEmpty(navEntry.subEntries)) {
      for (NavigationEntry subEntry : navEntry.subEntries) {
        processNavigationEntry(language, subEntry);
      }
    }
  }

  private NavigationEntry getSimplerNavigationEntry(NavigationEntry navEntry) {
    NavigationEntry result = new NavigationEntry();
    if (navEntry.href.equals("#")) {
      result.href = null;
      result.title = navEntry.title;
    } else {
      result.href = navEntry.href;
      result.title = null;
    }
    if (navEntry.subEntries != null) {
      List<NavigationEntry> newSubEntries = new ArrayList<>();
      for (NavigationEntry subEntry : navEntry.subEntries) {
        newSubEntries.add(getSimplerNavigationEntry(subEntry));
      }
      navEntry.subEntries = newSubEntries;
    }
    return result;
  }

  private NavigationEntry parseNavigationEntry(JsonNode n) {
    NavigationEntry nav = new NavigationEntry();
    if (n != null) {
      JsonNode hrefNode = n.get("href");
      if (hrefNode != null && hrefNode.isTextual()) {
        nav.href = hrefNode.asText();
      }
      JsonNode titleNode = n.get("title");
      if (titleNode != null && titleNode.isTextual()) {
        nav.title = n.get("title").asText();
      }
      JsonNode subPagesNode = n.get("subEntries");
      if (subPagesNode != null && subPagesNode.isArray()) {
        nav.subEntries = new ArrayList<>();
        subPagesNode.elements().forEachRemaining(subPageNode -> nav.subEntries.add(parseNavigationEntry(subPageNode)));
      }
    }
    return nav;
  }

  private Map<String, List<NavigationEntry>> parseNavigationJson(JsonNode n) {
    Map<String, List<NavigationEntry>> navMap = new HashMap<>();
    if (n != null && n.isObject()) {
      Iterator<String> iter = n.fieldNames();
      while (iter.hasNext()) {
        String language = iter.next();
        List<NavigationEntry> entries = new ArrayList<>();
        JsonNode navEntryNode = n.get(language);
        if (navEntryNode != null && navEntryNode.isArray()) {
          for (JsonNode itemNode : navEntryNode) {
            entries.add(parseNavigationEntry(itemNode));
          }
        }
        navMap.put(language, entries);
      }
    }
    return navMap;
  }

  public void load(ObjectMapper objectMapper, InputStream in) throws IOException {
    JsonNode node = objectMapper.readTree(in);
    Map<String, List<NavigationEntry>> sNav = parseNavigationJson(node);
    sNav.forEach((language, navEntries) -> navEntries.forEach((entry) -> {
      processNavigationEntry(language, entry);
    }));
    navigation.clear();
    navigation.putAll(sNav);
  }

  public void save(ObjectMapper objectMapper, OutputStream out) throws IOException {
    Map<String, List<NavigationEntry>> sNav = new HashMap<>();
    navigation.forEach((language, navEntries) -> {
      List<NavigationEntry> navEntryList = new ArrayList<>();
      navEntries.forEach(entry -> navEntryList.add(getSimplerNavigationEntry(entry)));
      sNav.put(language, navEntryList);
    });
    objectMapper.writeValue(out, sNav);
  }

}
