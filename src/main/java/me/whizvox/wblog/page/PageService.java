package me.whizvox.wblog.page;

import freemarker.template.Configuration;
import freemarker.template.TemplateException;
import me.whizvox.wblog.dbo.*;
import me.whizvox.wblog.i18n.Localizer;
import me.whizvox.wblog.util.StringUtils;
import org.jooq.DSLContext;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class PageService {

  private Localizer localizer;
  private PageRepository pageRepo;
  private BlogPostRepository blogPostRepo;
  private CachedPageRepository cachedPageRepo;
  private SyntaxParsingManager syntaxParsingManager;
  private TemplateManager templateManager;

  private PageService(Localizer localizer, PageRepository pageRepo, BlogPostRepository blogPostRepo, CachedPageRepository cachedPageRepo, SyntaxParsingManager syntaxParsingManager, TemplateManager templateManager) {
    this.localizer = localizer;
    this.pageRepo = pageRepo;
    this.blogPostRepo = blogPostRepo;
    this.cachedPageRepo = cachedPageRepo;
    this.syntaxParsingManager = syntaxParsingManager;
    this.templateManager = templateManager;
  }

  public Localizer getLocalizer() {
    return localizer;
  }

  public PageRepository getPageRepository() {
    return pageRepo;
  }

  public BlogPostRepository getBlogPostRepository() {
    return blogPostRepo;
  }

  public CachedPageRepository getCachedPageRepository() {
    return cachedPageRepo;
  }

  public SyntaxParsingManager getSyntaxParsingManager() {
    return syntaxParsingManager;
  }

  public TemplateManager getTemplateManager() {
    return templateManager;
  }

  public void createRepositories() {
    pageRepo.create();
    blogPostRepo.create();
    cachedPageRepo.create();
  }

  public enum AddPageResult {
    SUCCESS,
    PATH_LANGUAGE_CONFLICT,
    NO_PATH,
    NO_TITLE,
  }

  public AddPageResult addPage(Page page) {
    if (pageRepo.selectWithPathAndLanguage(page.path, page.language) != null) {
      return AddPageResult.PATH_LANGUAGE_CONFLICT;
    }
    if (StringUtils.isNullOrEmpty(page.path)) {
      return AddPageResult.NO_PATH;
    }
    if (StringUtils.isNullOrEmpty(page.title)) {
      return AddPageResult.NO_TITLE;
    }
    if (page.published == null) {
      page.published = new Date();
    }
    if (page.syntax == null) {
      page.syntax = "plain";
    }
    if (StringUtils.isNullOrEmpty(page.language)) {
      page.language = localizer.getFallbackLanguage();
    }
    pageRepo.insert(page);
    return AddPageResult.SUCCESS;
  }

  public enum UpdatePageResult {
    SUCCESS,
    ID_NOT_FOUND,
    PATH_LANGUAGE_CONFLICT,
    NO_PATH,
    NO_TITLE
  }

  public UpdatePageResult updatePage(Page page) {
    if (pageRepo.selectWithId(page.id) == null) {
      return UpdatePageResult.ID_NOT_FOUND;
    }
    if (pageRepo.selectWithPathAndLanguage(page.path, page.language) != null) {
      return UpdatePageResult.PATH_LANGUAGE_CONFLICT;
    }
    if (StringUtils.isNullOrEmpty(page.path)) {
      return UpdatePageResult.NO_PATH;
    }
    if (StringUtils.isNullOrEmpty(page.title)) {
      return UpdatePageResult.NO_TITLE;
    }
    if (StringUtils.isNullOrEmpty(page.syntax)) {
      page.syntax = "plain";
    }
    if (StringUtils.isNullOrEmpty(page.language)) {
      page.language = localizer.getFallbackLanguage();
    }
    page.lastEdited = new Date();

    pageRepo.update(page);
    return UpdatePageResult.SUCCESS;
  }

  public Page getPage(String path, String language) {
    Page page = pageRepo.selectWithPathAndLanguage(path, language);
    if (page == null) {
      page = pageRepo.selectWithPathAndLanguage(path, localizer.getFallbackLanguage());
    }
    if (page != null) {
      page.contents = syntaxParsingManager.parse(page.syntax, page.contents);
    }
    return page;
  }

  public enum AddBlogPostResult {
    SUCCESS,
    PATH_LANGUAGE_CONFLICT,
    NO_TITLE,
    NO_PATH,
    EMPTY_CONTENTS
  }

  public AddBlogPostResult addBlogPost(BlogPost post) {
    if (pageRepo.selectWithPathAndLanguage(post.path, post.language) != null) {
      return AddBlogPostResult.PATH_LANGUAGE_CONFLICT;
    }
    if (StringUtils.isNullOrEmpty(post.title)) {
      return AddBlogPostResult.NO_TITLE;
    }
    if (StringUtils.isNullOrEmpty(post.path)) {
      return AddBlogPostResult.NO_PATH;
    }
    if (StringUtils.isNullOrEmpty(post.contents)) {
      return AddBlogPostResult.EMPTY_CONTENTS;
    }
    if (post.published == null) {
      post.published = new Date();
    }
    if (StringUtils.isNullOrEmpty(post.language)) {
      post.language = localizer.getFallbackLanguage();
    }
    pageRepo.insert(post);
    blogPostRepo.insert(post);
    return AddBlogPostResult.SUCCESS;
  }

  public BlogPost getBlogPost(long id) {
    Page basePage = pageRepo.selectWithId(id);
    if (basePage == null) {
      return null;
    }
    BlogPost post = blogPostRepo.selectWithId(id);
    return BlogPost.fromPage(basePage, post);
  }

  public BlogPost getBlogPost(String path, String language) {
    Page pageBase = pageRepo.selectWithPathAndLanguage(path, language);
    if (pageBase != null) {
      BlogPost postSecond = blogPostRepo.selectWithId(pageBase.id);
      return BlogPost.fromPage(pageBase, postSecond);
    }
    return null;
  }

  public BlogPost getBlogPost(String path) {
    return getBlogPost(path, localizer.getFallbackLanguage());
  }

  public List<BlogPost> searchBlogPosts(String query, int maxSearchItems) {
    return pageRepo.searchGeneral(query, maxSearchItems).stream()
        .map(page -> {
          BlogPost post = blogPostRepo.selectWithId(page.id);
          return BlogPost.fromPage(page, post);
        })
        .filter(Objects::nonNull)
        .collect(Collectors.toList());
  }

  public CachedPage getCachedPage(String path, String language) {
    return cachedPageRepo.selectWithPathAndLanguage(path, language);
  }
  
  public CachedPage getRenderedPage(Page page, Object dataModel, boolean reload) throws TemplateException {
    if (reload) {

    }
    CachedPage cachedPage = getCachedPage(page.path, page.language);
    if (cachedPage == null) {
      cachedPage = getCachedPage(page.path, localizer.getFallbackLanguage());
      if (cachedPage == null) {
        cachedPage = reloadCachedPage(page, dataModel);
      } else {
        // temporarily set the cached page's language to the requested language, then put that into the database.
        // makes for less work when asking for the page again
        cachedPage.language = page.language;
        cachedPageRepo.insert(cachedPage);
        // set it back to the fallback once we're done
        cachedPage.language = localizer.getFallbackLanguage();
      }
    }
    return cachedPage;
  }

  private CachedPage reloadCachedPage(Page page, Object dataModel) throws TemplateException {
    String renderedContents = templateManager.process(page.template, dataModel); // should not be used very often
    // check if the cached page is in the database already
    CachedPage cachedPage = getCachedPage(page.path, page.language);
    if (cachedPage == null) {
      cachedPage = getCachedPage(page.path, localizer.getFallbackLanguage());
    }
    if (cachedPage == null) {
      // if it's not, then let's create a new one from scratch
      cachedPage = new CachedPage();
      cachedPage.language = page.language;
      cachedPage.path = page.path;
      cachedPage.title = page.title;
    } else {
      // if it is, then delete the old one
      cachedPageRepo.delete(cachedPage.id);
    }
    cachedPage.contents = renderedContents;
    cachedPage.lastUpdated = new Date();
    cachedPageRepo.insert(cachedPage);
    return cachedPage;
  }

  public void clearCache() {
    cachedPageRepo.deleteAll();
  }

  public static Builder builder() {
    return new Builder();
  }

  public static class Builder {
    public Localizer localizer;
    public DSLContext create;
    public PageRepository pageRepo;
    public BlogPostRepository blogPostRepo;
    public CachedPageRepository cachedPageRepo;
    public SyntaxParsingManager syntaxParsingManager;
    public TemplateManager templateManager;

    public Builder setLocalizer(Localizer localizer) {
      this.localizer = localizer;
      return this;
    }
    public Builder setDslContext(DSLContext create) {
      this.create = create;
      return this;
    }
    public Builder setPageRepository(PageRepository pageRepo) {
      this.pageRepo = pageRepo;
      return this;
    }
    public Builder setBlogPostRepository(BlogPostRepository blogPostRepo) {
      this.blogPostRepo = blogPostRepo;
      return this;
    }
    public Builder setCachedPageRepository(CachedPageRepository cachedPageRepo) {
      this.cachedPageRepo = cachedPageRepo;
      return this;
    }
    public Builder setSyntaxParsingManager(SyntaxParsingManager syntaxParsingManager) {
      this.syntaxParsingManager = syntaxParsingManager;
      return this;
    }
    public Builder setTemplateManager(TemplateManager templateManager) {
      this.templateManager = templateManager;
      return this;
    }

    public PageService build() {
      if (localizer == null) {
        throw new IllegalArgumentException("Localizer must be initialized");
      }
      if (create == null && (pageRepo == null || blogPostRepo == null || cachedPageRepo == null)) {
        throw new IllegalArgumentException("Repositories must be either set or supplied with a DSLContext");
      }
      if (syntaxParsingManager == null) {
        syntaxParsingManager = SyntaxParsingManager.builder().build();
      }
      if (templateManager == null) {
        templateManager = new TemplateManager(new Configuration(Configuration.VERSION_2_3_26));
      }
      if (pageRepo == null) {
        pageRepo = new PageRepository(create);
      }
      if (blogPostRepo == null) {
        blogPostRepo = new BlogPostRepository(create);
      }
      if (cachedPageRepo == null) {
        cachedPageRepo = new CachedPageRepository(create);
      }
      return new PageService(localizer, pageRepo, blogPostRepo, cachedPageRepo, syntaxParsingManager, templateManager);
    }
  }

}
