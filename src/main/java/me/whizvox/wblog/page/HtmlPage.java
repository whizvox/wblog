package me.whizvox.wblog.page;

import lombok.Getter;

import java.util.List;

public class HtmlPage {

  @Getter
  public String title;

  @Getter
  public String language;

  @Getter
  public String charset;

  @Getter
  public List<Meta> metadata;

  @Getter
  public List<Script> scripts;

  @Getter
  public List<Link> links;

  public void addStylesheet(String href, String media) {
    Link link = new Link();
    link.href = href;
    link.type = "text/stylesheet";
    link.rel = Link.Rel.STYLESHEET;
    link.media = media;
    links.add(link);
  }

  public void addStylesheet(String href) {
    addStylesheet(href, null);
  }

  public void addJSScript(String scriptBody, boolean defer, boolean async) {
    Script script = new Script();
    script.body = scriptBody;
    script.defer = defer;
    script.async = async;
    script.type = "application/javascript";
    scripts.add(script);
  }

  public void addJSScript(String scriptBody) {
    addJSScript(scriptBody, false, false);
  }

  public void addJSScriptSource(String source, String integrity, boolean defer, boolean async) {
    Script script = new Script();
    script.src = source;
    script.integrity = integrity;
    script.defer = defer;
    script.async = async;
    script.type = "application/javascript";
    scripts.add(script);
  }

  public void addJSScriptSource(String source) {
    addJSScriptSource(source, null, false, false);
  }

  public static class Meta {
    @Getter
    public String name;

    @Getter
    public String content;
  }

  public static class Script {
    @Getter
    public String body;

    @Getter
    public boolean async;

    @Getter
    public boolean defer;

    @Getter
    public boolean nomodule;

    @Getter
    public String src;

    @Getter
    public String type;

    @Getter
    public String integrity;
  }

  public static class Link {
    @Getter
    public String href;

    @Getter
    public Rel rel;

    @Getter
    public String type;

    @Getter
    public String hreflang;

    @Getter
    public String media;

    @Getter
    public Crossorigin crossorigin;

    @Getter
    public String sizes;

    public enum Rel {
      ALTERNATE,
      AUTHOR,
      DNS_PREFETCH("dns-prefetch"),
      HELP,
      ICON,
      LICENSE,
      NEXT,
      PINGBACK,
      PRECONNECT,
      PREFETCH,
      PRELOAD,
      PRERENDER,
      PREV,
      SEARCH,
      STYLESHEET;

      @Getter
      public final String str;

      Rel(String str) {
        this.str = str;
      }
      Rel() {
        str = toString().toLowerCase();
      }
    }

    public enum Crossorigin {
      ANONYMOUS,
      USE_CREDENTIALS("use-credentials");

      @Getter
      public final String str;

      Crossorigin(String str) {
        this.str = str;
      }
      Crossorigin() {
        str = toString().toLowerCase();
      }
    }

  }

}
