package me.whizvox.wblog.page;

import me.whizvox.wblog.Reference;
import me.whizvox.wblog.Wblog;
import spark.Filter;
import spark.Request;
import spark.Response;

import java.util.Map;

public class SetupLanguageFilter implements Filter {

  private Wblog wblog;

  public SetupLanguageFilter(Wblog wblog) {
    this.wblog = wblog;
  }

  @Override
  public void handle(Request request, Response response) throws Exception {
    String lang = request.session().attribute(Reference.SessionKeys.LANGUAGE);
    if (lang == null) {
      Map<String, String> cookies = request.cookies();
      lang = cookies.getOrDefault(Reference.CookieKeys.LANGUAGE, null);
      if (lang == null) {
        lang = wblog.getLocalizer().getFallbackLanguage();
        response.cookie(Reference.CookieKeys.LANGUAGE, lang);
      }
      request.session().attribute(Reference.SessionKeys.LANGUAGE, lang);
    }
  }

}
