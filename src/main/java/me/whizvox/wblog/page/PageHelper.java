package me.whizvox.wblog.page;

import java.util.ArrayList;

public class PageHelper {
  private PageHelper() {}

  public static HtmlPage setupHtmlPage() {
    HtmlPage htmlPage = new HtmlPage();
    htmlPage.charset = "UTF-8";
    htmlPage.links = new ArrayList<>();
    HtmlPage.Link mainCss = new HtmlPage.Link();
    mainCss.type = "text/css";
    mainCss.rel = HtmlPage.Link.Rel.STYLESHEET;
    mainCss.href = "stylesheet/main.css";
    htmlPage.links.add(mainCss);
    htmlPage.metadata = new ArrayList<>();

    return htmlPage;
  }

  /*public static NavigablePage setupNavigablePage() {

  }*/

}
