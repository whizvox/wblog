package me.whizvox.wblog.page;

public interface SyntaxParser {

  String parse(String contents);

}
