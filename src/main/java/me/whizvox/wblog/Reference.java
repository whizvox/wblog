package me.whizvox.wblog;

import me.whizvox.wblog.util.StringUtils;
import me.whizvox.wblog.version.Version;

import java.util.Date;

public class Reference {

  private static final String
      __VERSION_STRING = "@VERSION@",
      __RELEASED_STRING = "@RELEASED@";
  public static final Version VERSION;
  public static final Date RELEASED;

  static {
    // don't want the filter to pick this up
    if (__VERSION_STRING.equals("\u0040VERSION\u0040")) {
      VERSION = new Version(0, 0, 0, "dev", 0);
    } else {
      VERSION = Version.DEV_VERSION;
    }
    if (__RELEASED_STRING.equals("\u0040RELEASED\u0040")) {
      RELEASED = new Date();
    } else {
      RELEASED = StringUtils.parseDate(__RELEASED_STRING);
    }
  }

  public static boolean isDevVersion() {
    return VERSION.equals(Version.DEV_VERSION);
  }

  public static final String UPDATE_CHECK_URL = "https://raw.githubusercontent.com/whizvox/wblog/master/latestVersion.json";

  public static final String DEFAULT_FALLBACK_LANGUAGE = "en";

  public static final class CookieKeys {
    public static final String
        LOGIN_TOKEN = "loginToken",
        LANGUAGE = "language";
  }

  public static final class SessionKeys {
    public static final String
        USER = "user",
        LANGUAGE = "language";
  }

  public static final class SettingKeys {
    public static final String
        CHECK_UPDATES = "wblog.updates.check",
        SPARK_STATIC_FILES_LOCATION = "server.spark.staticFilesLocation",
        SPARK_PORT = "server.spark.port",
        JDBC_URL = "server.jdbc.url",
        JDBC_USERNAME = "server.jdbc.username",
        JDBC_PASSWORD = "server.jdbc.password",
        SERVER_RESTART_AUTO = "server.restart.auto",
        SERVER_RESTART_INTERVAL = "server.restart.interval",
        TEMPLATE_DIRECTORY = "server.templates.baseDirectory",
        SITE_NAVIGATION_USE_HORIZONTAL = "site.navigation.useHorizontal",
        SITE_LANDING_PAGE = "site.general.landingPage",
        SITE_TITLE_FORMAT = "site.general.titleFormat",
        SITE_NAME = "site.general.name",
        SITE_DESCRIPTION = "site.general.description",
        SITE_KEYWORDS = "site.general.keywords",
        SITE_FOOTER = "site.general.footer";
  }

  public static final class LanguageKeys {
    // TODO: Maybe I don't need to define most of these?
    public static final String
        DEFAULT_FALLBACK = "en",
        NAME = "lang.name",
        SITE_GENERAL_FOOTER = "site.general.footer",
        SITE_GENERAL_DESCRIPTION = "site.general.description",
        SITE_GENERAL_KEYWORDS = "site.general.keywords",
        REGISTER_USER_HEADER = "site.register.header",
        REGISTER_USER_USERNAME = "site.register.username",
        REGISTER_USERNAME_TAKEN = "site.register.usernameTaken",
        REGISTER_USER_EMAIL = "site.register.email",
        REGISTER_USER_CONFIRM_EMAIL = "site.register.confirmEmail",
        REGISTER_USER_EMAIL_TAKEN = "site.register.emailTaken",
        REGISTER_USER_PASSWORD = "site.register.password",
        REGISTER_USER_CONFIRM_PASSWORD = "site.register.confirmPassword",
        REGISTER_USER_INVALID_PASSWORD = "site.register.invalidPassword",
        REGISTER_USER_SUBMIT = "site.register.submit";
  }

  public static final class ParamKeys {
    private ParamKeys() {}

    public static final String
        PAGE_NAME = "page",
        INVALID_PASSWORD = "badPassword",
        INVALID_EMAIL = "badEmail",
        EMAIL_TAKEN = "emailTaken",
        USERNAME_TAKEN = "usernameTaken",
        CAPTCHA_WRONG = "captchaWrong";
  }

  public static final class FilePaths {
    public static final String
        ROOT_DIR = ".",
        SETTINGS = "wblog.settings",
        SETTINGS_JSON = "wblog.settings.json",
        NAVIGATION = "navigation.json",
        LANG_DIR = "lang",
        LANGUAGES_LIST = "lang/languages.json",
        CACHE_DIR = "cache",
        GROUPS = "groups.json";
  }

}
