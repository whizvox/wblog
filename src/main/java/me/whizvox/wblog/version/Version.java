package me.whizvox.wblog.version;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Version implements Comparable<Version> {

  private int major;
  private int minor;
  private int revision;
  private String type;
  private int build;

  public Version(int major, int minor, int revision, String type, int build) {
    this.type = type;
    this.major = major;
    this.minor = minor;
    this.revision = revision;
    this.build = build;
  }

  public Version(int major, int minor, int revision) {
    this(major, minor, revision, null, 0);
  }

  public boolean isFull() {
    return type != null && build > 0;
  }

  @Override
  public String toString() {
    if (isFull()) {
      return major + "." + minor + "." + revision + "-" + type + "-" + build;
    }
    return major + "." + minor + "." + revision;
  }

  @Override
  public int compareTo(Version other) {
    if (isFull() && other.isFull()) {
      return build - other.build;
    }
    int res = major - other.major;
    if (res == 0) {
      res = minor - other.minor;
      if (res == 0) {
        res = revision - other.revision;
      }
    }
    return res;
  }

  @Override
  public boolean equals(Object other) {
    if (other instanceof Version) {
      Version otherVersion = (Version) other;
      if (isFull() && otherVersion.isFull()) {
        return major == otherVersion.major &&
            minor == otherVersion.minor &&
            revision == otherVersion.revision &&
            type.equals(otherVersion.type) &&
            build == otherVersion.build;
      }
      return major == otherVersion.major &&
          minor == otherVersion.minor &&
          revision == otherVersion.revision;
    }
    return false;
  }

  public static final Version DEV_VERSION = new Version(0, 0, 0, "dev", 0);

  // 0.1.0-pre-95
  private static final Pattern PATTERN_FULL = Pattern.compile("^(\\d).(\\d).(\\d)-([a-zA-Z0-9_]+)-(\\d)$");

  // 0.1.0
  private static final Pattern PATTERN_MINIMAL = Pattern.compile("^(\\d).(\\d).(\\d)$");

  public static Version parse(String str) {
    if (str != null && !str.isEmpty()) {
      Matcher m = PATTERN_FULL.matcher(str);
      if (m.find()) {
        try {
          int major = Integer.parseInt(m.group(1));
          int minor = Integer.parseInt(m.group(2));
          int revision = Integer.parseInt(m.group(3));
          String type = m.group(4);
          int build = Integer.parseInt(m.group(5));
          return new Version(major, minor, revision, type, build);
        } catch (NumberFormatException | IndexOutOfBoundsException ignored) {}
      } else {
        m = PATTERN_MINIMAL.matcher(str);
        if (m.find()) {
          try {
            int major = Integer.parseInt(m.group(1));
            int minor = Integer.parseInt(m.group(2));
            int revision = Integer.parseInt(m.group(3));
            return new Version(major, minor, revision);
          } catch (NumberFormatException | IndexOutOfBoundsException ignored) {}
        }
      }
    }
    return null;
  }

}
