package me.whizvox.wblog.version;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import me.whizvox.wblog.util.StringUtils;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;

public class UpdateChecker {

  private ObjectMapper mapper;
  private URL checkUrl;

  public UpdateChecker(ObjectMapper mapper, String checkUrl) throws MalformedURLException {
    if (mapper == null) {
      this.mapper = new ObjectMapper();
    } else {
      this.mapper = mapper;
    }
    this.checkUrl = new URL(checkUrl);
  }

  public UpdateChecker(String checkUrl) throws MalformedURLException {
    this(null, checkUrl);
  }

  // Example JSON text:
  //
  // {
  //   "version": "1.0.0-LTR-1024",
  //   "when": "2019/07/07 12:10:00",
  //   "message": "Implemented working thingy.",
  //   "download": "https://somedownloadlinkhere.whatever"
  // }
  public Result check() throws IOException {
    Result r = new Result();
    JsonNode n = mapper.readTree(checkUrl);
    if (n != null) {
      if (n.has("version") && n.has("when")) {
        r.version = Version.parse(n.get("version").asText());
        r.when = StringUtils.parseDate(n.get("when").asText());
        // TODO: Maybe don't have a message component? Another string to localize.
        if (n.has("message")) {
          r.message = n.get("message").asText();
        }
        if (n.has("download")) {
          r.download = n.get("download").asText();
        }
        r.type = Result.Type.SUCCESS;
        return r;
      } else {
        r.type = Result.Type.INCOMPLETE;
      }
    } else {
      r.type = Result.Type.MALFORMED;
    }
    return r;
  }

  public static class Result {
    public Version version;
    public Date when;
    public String message;
    public String download;
    public Type type;

    public enum Type {
      SUCCESS,
      MALFORMED,
      INCOMPLETE
    }
  }

  public enum Status {
    UP_TO_DATE,
    UPDATE_AVAILABLE,
    DEV_BUILD,
    INDETERMINATE
  }

}
