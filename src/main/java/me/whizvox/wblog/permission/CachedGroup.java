package me.whizvox.wblog.permission;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CachedGroup extends Group {

  private Map<Permission, Boolean> cache;

  CachedGroup(List<Permission> permissions) {
    super(permissions);
    cache = new HashMap<>();
  }

  public CachedGroup(Group group) {
    this(group.permissions);
  }

  public void clearCache() {
    cache.clear();
  }

  @Override
  public boolean hasPermission(Permission permission) {
    Boolean b = cache.getOrDefault(permission, null);
    if (b == null) {
      b = super.hasPermission(permission);
      cache.put(permission, b);
    }
    return b;
  }

  public void save(ObjectMapper mapper, OutputStream out) throws IOException {
    mapper.writeValue(out, cache);
  }

  public void load(ObjectMapper mapper, InputStream in) throws IOException {
    JsonNode n = mapper.readTree(in);
    clearCache();
    if (n.isArray()) {
      n.fieldNames().forEachRemaining(f -> {
        JsonNode item = n.get(f);
        if (item.isBoolean()) {
          cache.put(Permission.parse(f), item.asBoolean());
        }
      });
    }
  }

  private static final class NopCachedGroup extends CachedGroup {
    public NopCachedGroup(Group group) {
      super(group);
    }
    @Override
    public boolean hasPermission(Permission permission) {
      return false;
    }
  }

  public static final CachedGroup NOP_CACHED_GROUP = new NopCachedGroup(Group.NOP_GROUP);

}
