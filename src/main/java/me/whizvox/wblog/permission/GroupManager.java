package me.whizvox.wblog.permission;

import com.fasterxml.jackson.databind.ObjectMapper;
import me.whizvox.wblog.Wblog;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.*;
import java.util.stream.Collectors;

public class GroupManager implements Iterable<String> {

  private Map<String, CachedGroup> groups;
  private int bGroupsHash;
  private List<BaseGroup> bGroups;

  public GroupManager() {
    groups = new HashMap<>();
    bGroups = new ArrayList<>();
    bGroupsHash = 0;
  }

  public Group getGroup(String name) {
    return groups.getOrDefault(name, CachedGroup.NOP_CACHED_GROUP);
  }

  public boolean hasPermission(String groupName, Permission p) {
    return getGroup(groupName).hasPermission(p);
  }

  public boolean hasPermission(String groupName, String permission) {
    return hasPermission(groupName, Permission.parse(permission));
  }

  private boolean doesNotRecurse(List<String> inheritChain, BaseGroup bg) {
    if (bg.parent == null) {
      return true;
    }
    if (inheritChain.contains(bg.parent)) {
      return false;
    }
    BaseGroup parent = bGroups.stream().filter(baseGroup -> baseGroup.name.equals(bg.parent)).findAny().orElse(null);
    if (parent == null) {
      return true;
    }
    inheritChain.add(parent.name);
    return doesNotRecurse(inheritChain, parent);
  }

  // We want to go through the permissions in a specific order: parents (and siblings) then children (and siblings).
  // For example: if group "admin" depends on group "moderator" which depends on group "member", then we want to
  // iterate through them in this order: ["member", "moderator", "admin"], starting with "member", assuming that group
  // has no parents.
  private void reload_do() {
    groups.clear();
    // filter out all "invalid" groups that result in infinite recursion of inheritance
    List<BaseGroup> nBGroups = bGroups.stream().filter(bg -> {
      List<String> inheritChain = new ArrayList<>();
      return doesNotRecurse(inheritChain, bg);
    }).collect(Collectors.toList());

    // start out with the groups that have no parents
    List<BaseGroup> orderedBGroups = new ArrayList<>();
    nBGroups.forEach(bg -> {
      if (bg.parent == null) {
        orderedBGroups.add(bg);
      }
    });

    // add all groups in a hierarchy-esque order
    while (orderedBGroups.size() < bGroups.size()) {
      for (BaseGroup bg : nBGroups) {
        if (!orderedBGroups.contains(bg)) {
          // only add to the final list if the group's parent exists and is in the ordered list
          if (bGroups.stream().noneMatch(obg -> obg.name.equals(bg.parent)) ||
              orderedBGroups.stream().anyMatch(obg -> obg.name.equals(bg.parent))) {
            orderedBGroups.add(bg);
            break;
          }
        }
      }
    }

    // Finally, parse all permissions and add them to the final groups list
    orderedBGroups.forEach(bg -> {
      List<Permission> permissions = new ArrayList<>();
      bg.permissions.forEach(pStr -> {
        Permission p = Permission.parse(pStr);
        if (pStr != null) {
          permissions.add(p);
        }
      });
      permissions.sort(Permission::compareTo);
      Group parent = null;
      if (bg.parent != null) {
        parent = groups.getOrDefault(bg.parent, null);
        if (parent == null) {
          Wblog.LOGGER.error("Invalid parent group found when scanning permissions <group=%s, parent=%s>", bg.name, bg.parent);
        }
      }
      groups.put(bg.name, new CachedGroup(Group.get(permissions, parent)));
    });
    bGroups.stream().filter(bg -> !orderedBGroups.contains(bg))
        .forEachOrdered(bg ->
            Wblog.LOGGER.error("Invalid group found, as its inheritance results in infinite recursion <group=%s, parent=%s>",
                bg.name, bg.parent));
  }

  public void reload() {
    if (bGroups.hashCode() != bGroupsHash) {
      reload_do();
      bGroupsHash = bGroups.hashCode();
    }
  }

  public void load(ObjectMapper objectMapper, InputStream in) throws IOException {
    this.bGroups = objectMapper.readValue(in,
        objectMapper.getTypeFactory().constructCollectionType(List.class, BaseGroup.class));
    reload_do();
  }

  public void save(ObjectMapper objectMapper, OutputStream out) throws IOException {
    objectMapper.writeValue(out, bGroups);
  }

  @Override
  public Iterator<String> iterator() {
    return groups.keySet().iterator();
  }

  private static class BaseGroup {
    public String name;
    public String parent;
    public List<String> permissions;
  }

}
