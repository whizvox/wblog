package me.whizvox.wblog.permission;

import java.util.*;

public class Group implements Iterable<Permission> {

  List<Permission> permissions;

  Group(List<Permission> permissions) {
    this.permissions = permissions;
  }

  public boolean hasPermission(Permission permission) {
    boolean b = false;
    for (Permission p : permissions) {
      if (p.grants(permission)) {
        b = true;
      } else if (p.limitedGroupsEquals(permission)) {
        b = false;
        break;
      }
    }
    return b;
  }

  @Override
  public Iterator<Permission> iterator() {
    return permissions.iterator();
  }

  public static final Group NOP_GROUP = new Group(Collections.emptyList());

  public static Group get(List<Permission> basePermissions, Group parent) {
    List<Permission> permissions = new ArrayList<>(basePermissions);
    permissions.sort(Comparator.comparingInt(Permission::groupCount));
    if (parent != null) {
      Set<Permission> discardedPermissions = new HashSet<>();
      for (Permission bp : basePermissions) {
        for (Permission pp : parent.permissions) {
          if (bp.limitedGroupsEquals(pp)) {
            discardedPermissions.add(pp);
          } else if (!discardedPermissions.contains(pp) && !permissions.contains(pp)) {
            permissions.add(pp);
          }
        }
      }
    }
    return new Group(permissions);
  }

}
