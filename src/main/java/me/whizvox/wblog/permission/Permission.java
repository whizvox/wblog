package me.whizvox.wblog.permission;

import me.whizvox.wblog.util.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * A permission. Used to store a permission's groups, whether they're negated or not, and can be used to determine
 * whether it grants other permissions.
 * <p>
 *   A permission can be represented as a string. Like so:
 *   <pre>site.user.edit.others</pre>
 *   where "site", "user", "edit", and "others" are groups of the permission.
 * </p>
 * <p>
 *   A permission can also be negated, like so:
 *   <pre>-site.user.edit.others</pre>
 *   This permission will not grant a permission of the same name.
 * </p>
 * <p>
 *   Wildcards are also a supported function. They're not defined explicitly, but a permission will grant all other
 *   permissions if its base groups equal that of the other permission. Example:
 *   <pre>
 *     Permission p1 = Permission.parse("site.user");
 *     Permission p2 = Permission.parse("site.user.edit.others");
 *     boolean grants = p1.grants(p2); // -> true
 *   </pre>
 *   Since both permissions start with "site.user", p2 will fall under p1's jurisdiction, acting like a "child" of p1.
 *   Negated wildcards are also supported:
 *   <pre>
 *     Permission p1 = Permission.parse("-site.user.edit");
 *     Permission p2 = Permission.parse("site.user.edit.others");
 *     boolean grants = p1.grants(p2); // -> false
 *   </pre>
 * </p>
 * <p>
 *   A permission is represented as a string. Looks something like this:
 *   <pre>site.user.edit.others</pre>
 *   where site, edit, and others are known as "groups" of the permission.
 * </p>
 * <p>
 *   Heavily inspired by the CraftBukkit permissions system.
 * </p>
 *
 * @see #grants(Permission)
 */
public class Permission implements Comparable<Permission> {

  private final boolean negated;
  private final String[] groups;
  private final String asString;

  private Permission(boolean negated, String[] groups) {
    this.negated = negated;
    this.groups = groups;
    StringBuilder sb = new StringBuilder();
    if (negated) {
      sb.append('-');
    }
    if (groups.length > 0) {
      sb.append(groups[0]);
      for (int i = 1; i < groups.length; i++) {
        sb.append('.').append(groups[i]);
      }
    }
    asString = sb.toString();
  }

  public boolean limitedGroupsEquals(Permission p) {
    int limit = Math.min(p.groups.length, groups.length);
    for (int i = 0; i < limit; i++) {
      if (!groups[i].equals(p.groups[i])) {
        return false;
      }
    }
    return true;
  }

  /**
   * Whether this permission grants the passed permission.
   * <p>
   *   A permission will "grant" another if all conditions are met:
   *   <ol>
   *     <li>Both permissions' groups are equal in content, limited by the smaller groups length.</li>
   *     <li>The passed permission has the same negation as this permission.</li>
   *   </ol>
   *   A permission is NOT granted if one of the following conditions are met:
   *   <ul>
   *     <li>The passed permission has less groups than this permission.</li>
   *     <li>The groups of both permissions are not equal in content.</li>
   *   </ul>
   * </p>
   * @param p The passed permssion. Is checked against this permission.
   * @return
   */
  public boolean grants(Permission p) {
    if (p == null || groups.length > p.groups.length) {
      return false;
    }
    boolean limEq = limitedGroupsEquals(p);
    return limEq && negated == p.negated;
  }

  public final int groupCount() {
    return groups.length;
  }

  @Override
  public String toString() {
    return asString;
  }

  @Override
  public boolean equals(Object o) {
    if (o instanceof Permission) {
      return o.toString().equals(toString());
    }
    return false;
  }

  @Override
  public int hashCode() {
    return asString.hashCode();
  }

  public static Permission parse(String str) {
    if (str == null || str.isEmpty() || (str.length() == 1 && str.charAt(0) == '-')) {
      return null;
    }
    List<String> groups = new ArrayList<>();
    boolean negated = false;
    if (str.charAt(0) == '-') {
      negated = true;
      str = str.substring(1);
    }
    int last = 0;
    for (int i = 0; i < str.length(); i++) {
      if (str.charAt(i) == '.') {
        groups.add(str.substring(last, i).toLowerCase());
        last = i + 1;
      }
    }
    groups.add(str.substring(last));
    return new Permission(negated, StringUtils.arrayFromList(groups));
  }

  @Override
  public int compareTo(Permission permission) {
    return groups.length - permission.groups.length;
  }

}

