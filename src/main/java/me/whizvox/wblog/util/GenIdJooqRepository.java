package me.whizvox.wblog.util;

import org.jooq.DSLContext;
import org.jooq.Table;

import java.util.concurrent.atomic.AtomicLong;

public class GenIdJooqRepository extends JooqRepository {

  private AtomicLong idCounter;

  public GenIdJooqRepository(DSLContext create, Table table) {
    super(create, table);
    idCounter = new AtomicLong(0L);
  }

  protected void setNextAvailableId(long id) {
    idCounter.set(id);
  }

  protected long getNextAvailableId() {
    return idCounter.get();
  }

  protected void increaseNextAvailableId() {
    idCounter.addAndGet(1);
  }

}
