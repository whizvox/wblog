package me.whizvox.wblog.util;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.temporal.TemporalUnit;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class DateUtils {

  public static Date getOffsetFromNow(TemporalUnit unit, long num) {
    return new Date(LocalDateTime.now().plus(num, unit).toEpochSecond(ZoneOffset.UTC));
  }

  public static Date getDaysOffsetFromNow(int days) {
    Calendar calendar = new GregorianCalendar();
    calendar.setTime(new Date());
    calendar.add(Calendar.DAY_OF_YEAR, days);
    return calendar.getTime();
  }

}
