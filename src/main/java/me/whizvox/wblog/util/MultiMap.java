package me.whizvox.wblog.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MultiMap<KEY, VALUE> extends HashMap<KEY, List<VALUE>> {

  public MultiMap(int initialCapacity, float loadFactor) {
    super(initialCapacity, loadFactor);
  }

  public MultiMap(int initialCapacity) {
    super(initialCapacity);
  }

  public MultiMap() {
  }

  public MultiMap(Map<? extends KEY, ? extends List<VALUE>> m) {
    super(m);
  }

  public void add(KEY key, VALUE value) {
    if (!containsKey(key)) {
      List<VALUE> list = new ArrayList<>();
      list.add(value);
      put(key, list);
    } else {
      get(key).add(value);
    }
  }

  public VALUE getFirst(KEY key) {
    if (!containsKey(key)) {
      return null;
    }
    return get(key).get(0);
  }

  public static <KEY, VALUE> MultiMap<KEY, VALUE> fromMap(Map<KEY, VALUE> map) {
    MultiMap<KEY, VALUE> result = new MultiMap<>();
    map.forEach(result::add);
    return result;
  }

}
