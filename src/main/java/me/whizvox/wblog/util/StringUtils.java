package me.whizvox.wblog.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringUtils {

  private static final Pattern PATTERN_EMAIL = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,63}$");

  public static boolean isValidEmailAddress(String emailAddress) {
    return PATTERN_EMAIL.matcher(emailAddress).matches();
  }

  private static final SimpleDateFormat DATE_FORMAT_DEFAULT = new SimpleDateFormat("yyyy/MM/dd kk:mm:ss");

  public static Date parseDate(String str) {
    try {
      return DATE_FORMAT_DEFAULT.parse(str);
    } catch (ParseException ignored) {}
    return null;
  }

  public static String getHumanFriendlyDate(Date date) {
    return DATE_FORMAT_DEFAULT.format(date);
  }

  private static final SimpleDateFormat DATE_FORMAT_FILE = new SimpleDateFormat("yyyyMMdd-kkmmss");

  public static Date parseFileFriendlyDate(String s) {
    try {
      return DATE_FORMAT_FILE.parse(s);
    } catch (ParseException ignored) {}
    return null;
  }

  public static String getFileFriendlyDate(Date date) {
    return DATE_FORMAT_FILE.format(date);
  }

  private static char[] HEX_CHARS = new char[] {'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};

  public static String bytesToHex(byte[] bytes) {
    if (bytes == null || bytes.length == 0) {
      return "";
    }
    char[] chars = new char[bytes.length * 2];
    for (int i = 0; i < bytes.length; i++) {
      chars[i * 2] = HEX_CHARS[(bytes[i] & 0xF0) >>> 4];
      chars[i * 2 + 1] = HEX_CHARS[bytes[i] & 0x0F];
    }
    return new String(chars);
  }

  public static byte[] hexToBytes(String hex) {
    if (hex == null || hex.isEmpty()) {
      return new byte[0];
    }
    // if the string is odd-lengthed, then add an extra 0 to the front
    if (hex.length() % 2 != 0) {
      hex = "0" + hex;
    }
    byte[] bytes = new byte[hex.length() / 2];
    for (int i = 0; i < bytes.length; i++) {
      bytes[i] = (byte)
          ((Character.digit(hex.charAt(i * 2), 16) & 0xF) << 4 |
          (Character.digit(hex.charAt(i * 2 + 1), 16) & 0xF));
    }
    return bytes;
  }

  public static void tokenize(String str, char separator, List<String> output, int max) {
    if (str == null || str.isEmpty()) {
      return;
    }
    int last = 0;
    int n = 0;
    for (int i = 0; n < max - 1 && i < str.length(); i++) {
      if (str.charAt(i) == separator) {
        output.add(str.substring(last, i));
        last = i + 1;
        n++;
      }
    }
    output.add(str.substring(last));
  }

  public static void tokenize(String str, char separator, List<String> output) {
    if (str == null || str.isEmpty()) {
      return;
    }
    int last = 0;
    for (int i = 0; i < str.length(); i++) {
      if (str.charAt(i) == separator) {
        output.add(str.substring(last, i));
        last = i + 1;
      }
    }
    output.add(str.substring(last));
  }

  public static boolean containsChar(String str, char c) {
    if (str == null || str.isEmpty()) {
      return false;
    }
    for (int i = 0; i < str.length(); i++) {
      if (str.charAt(i) == c) {
        return true;
      }
    }
    return false;
  }

  public static boolean parseBoolean(String str) {
    if (str != null && !str.isEmpty()) {
      if (str.equals("0") || str.equalsIgnoreCase("false")) {
        return false;
      }
      if (str.equals("1") || str.equalsIgnoreCase("true")) {
        return true;
      }
    }
    throw new NumberFormatException("Not a boolean: " + str);
  }

  public static char parseChar(String str) {
    if (str != null && str.length() == 1) {
      return str.charAt(0);
    }
    throw new NumberFormatException("Not a char: " + str);
  }

  public static boolean isNullOrEmpty(String s) {
    return s == null || s.isEmpty();
  }

  public static boolean isNullOrEmpty(Collection<?> collection) {
    return collection == null || collection.isEmpty();
  }

  public static String[] arrayFromList(List<String> list) {
    return list.toArray(new String[0]);
  }

  private static final Pattern DELIMITER_BREAKUP = Pattern.compile("\"\"([^\"]*)\"|(\\\\S+)\"");

  public static List<String> tokenizeTags(String tags, boolean trimIfNeeded) {
    if (!trimIfNeeded) {
      List<String> result = new ArrayList<>();
      tokenize(tags, ',', result);
      return result;
    }
    List<String> result = new ArrayList<>();
    List<String> tokens = new ArrayList<>();
    tokenize(tags, ',', tokens);
    tokens.forEach(token -> result.add(token.trim()));
    return result;
  }

  public static String tagsToString(String[] tags) {
    if (tags.length == 0) {
      return "";
    }
    StringBuilder sb = new StringBuilder();
    sb.append(tags[0]);
    for (int i = 1; i < tags.length; i++) {
      sb.append(',').append(tags[i]);
    }
    return sb.toString();
  }

  public static List<String> breakupArgumentTokens(String query) {
    Matcher m = DELIMITER_BREAKUP.matcher(query);
    List<String> items = new ArrayList<>();
    while (m.find()) {
      if (m.group(1) != null) {
        items.add(m.group(1));
      } else {
        items.add(m.group(2));
      }
    }
    return items;
  }

  public static List<String> breakupArgumentTokens(String query, int max) {
    List<String> tokens = breakupArgumentTokens(query);
    if (tokens.size() > max) {
      tokens.clear();
      tokens.add(query);
    }
    return tokens;
  }

  public static String exceptionToString(Throwable t) {
    // recommended
    try (ByteArrayOutputStream out = new ByteArrayOutputStream()) {
      t.printStackTrace(new PrintStream(out));
      return out.toString();
    } catch (IOException ignored) {}

    // legacy
    StringBuilder sb = new StringBuilder();
    sb.append(t.getClass()).append(": ").append(t.getMessage()).append("\n\r");
    for (StackTraceElement e : t.getStackTrace()) {
      sb.append('\t').append(e.toString()).append("\r\n");
    }
    return sb.toString();
  }

  public static String getFileExtension(String fileName) {
    int indexOf = fileName.lastIndexOf('.');
    if (indexOf > 0) {
      return fileName.substring(indexOf);
    }
    return "";
  }

  public static String getFileNameWithoutExtension(String fileName) {
    int indexOf = fileName.lastIndexOf('.');
    if (indexOf > 0) {
      return fileName.substring(0, indexOf);
    }
    return fileName;
  }

  public static boolean argumentsContains(String[] args, String token) {
    for (String arg : args) {
      if (arg.equalsIgnoreCase(token)) {
        return true;
      }
    }
    return false;
  }

  private static final Pattern PATTERN_PAGE_NAME = Pattern.compile("^[a-zA-Z0-9_-]+$");

  public static boolean validPageName(String name) {
    return PATTERN_PAGE_NAME.matcher(name).matches();
  }

  public static String cleanupPageName(String origName) {
    StringBuilder sb = new StringBuilder(origName.length());
    origName.chars().forEachOrdered(i -> {
      if (Character.isAlphabetic(i) || Character.isDigit(i) || i == '_' || i == '-') {
        sb.append((char) i);
      } else {
        sb.append('_');
      }
    });
    return sb.toString();
  }

}
