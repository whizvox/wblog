package me.whizvox.wblog.util;

public class Pair<E1, E2> {

  public final E1 firstElement;
  public final E2 secondElement;

  public Pair(E1 firstElement, E2 secondElement) {
    this.firstElement = firstElement;
    this.secondElement = secondElement;
  }

}
