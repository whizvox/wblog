package me.whizvox.wblog.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.function.Consumer;

public class LoggerListenerManager implements AutoCloseable, Runnable {

  private Path output;
  private long lastModified;
  private long delay;
  private Map<String, Consumer<Iterable<String>>> listeners;
  private BufferedReader reader;
  private boolean poll;

  public LoggerListenerManager(Path logPath, long delay) throws IOException {
    this.output = logPath;
    lastModified = 0;
    this.delay = delay;
    listeners = new HashMap<>();
    reader = Files.newBufferedReader(logPath);
    poll = true;
  }

  public LoggerListenerManager(Path logPath) throws IOException {
    this(logPath, 1000);
  }

  public synchronized boolean addListener(String name, Consumer<Iterable<String>> listener) {
    if (listeners.containsKey(name)) {
      return false;
    }
    listeners.put(name, listener);
    return true;
  }

  public synchronized void stopPolling() {
    poll = false;
  }

  @Override
  public void close() throws Exception {
    reader.close();
    poll = false;
  }

  private void post(Iterable<String> lines) {
    listeners.values().forEach(c -> c.accept(lines));
  }

  private void loop() throws IOException {
    long lm = output.toFile().lastModified();
    if (lm != lastModified) {
      lastModified = lm;
      List<String> lines = new ArrayList<>();
      String line;
      while ((line = reader.readLine()) != null) {
        lines.add(line);
      }
      post(lines);
    }
  }

  @Override
  public void run() {
    while (poll) {
      try {
        loop();
        Thread.sleep(delay);
      } catch (IOException | InterruptedException e) {
        post(Collections.singletonList(StringUtils.exceptionToString(e)));
      }
    }
  }

}
