package me.whizvox.wblog.util;

import freemarker.cache.URLTemplateLoader;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.*;
import java.util.HashMap;
import java.util.Map;

public class AutoCopyTemplateLoader extends URLTemplateLoader {

  private ClassLoader classLoader;
  private String basePath;
  private Path baseOutputDir;
  private Map<String, URL> resourceCache;

  public AutoCopyTemplateLoader(ClassLoader classLoader, String basePath, Path baseOutputDir) {
    this.classLoader = classLoader;
    if (basePath == null || basePath.equals("/") || basePath.equals("\\")) {
      basePath = "";
    }
    this.basePath = basePath;
    this.baseOutputDir = baseOutputDir;
    resourceCache = new HashMap<>();
  }

  public AutoCopyTemplateLoader(Class<?> cls, String basePath, Path baseOutputDir) {
    this(cls.getClassLoader(), basePath, baseOutputDir);
  }

  private String resolve(String name) {
    if (basePath.isEmpty()) {
      return name;
    }
    return basePath + "/" + name;
  }

  public URL cache(String name) throws IOException {
    String resolvedPathString = resolve(name);
    URL url = classLoader.getResource(resolvedPathString);
    if (url == null) {
      throw new FileNotFoundException("Internal resource not found: " + name);
    }
    Path path = baseOutputDir.resolve(resolvedPathString);
    if (!Files.exists(path.getParent())) {
      Files.createDirectories(path.getParent());
    }
    try (InputStream in = url.openStream()) {
      Files.copy(in, path, StandardCopyOption.REPLACE_EXISTING);
    }
    URL result = path.toUri().toURL();
    resourceCache.put(name, result);
    return result;
  }

  @Override
  protected URL getURL(String name) {
    URL result = resourceCache.get(name);
    if (result != null) {
      return result;
    }
    try {
      return cache(name);
    } catch (IOException e) {
      e.printStackTrace();
    }
    return null;
  }

}
