package me.whizvox.wblog.util;

import java.util.concurrent.TimeUnit;

public abstract class Stopwatch {

  private long start, end;
  private long pause;
  private long totalPauseTime;

  public Stopwatch() {
    reset();
  }

  public abstract long getTime();

  public void reset() {
    start = 0;
    end = 0;
    pause = 0;
    totalPauseTime = 0;
  }

  public boolean isRunning() {
    return start != 0 && end == 0 && pause == 0;
  }

  public boolean isPaused() {
    return pause != 0;
  }

  public long getElapsedTime() {
    if (!isRunning()) {
      return (end - start) - totalPauseTime;
    }
    return (getTime() - start) - totalPauseTime;
  }

  public boolean start() {
    if (!isRunning() && !isPaused()) {
      reset();
      start = getTime();
      return true;
    }
    return false;
  }

  public boolean pause() {
    if (isRunning() && !isPaused()) {
      pause = getTime();
      return true;
    }
    return false;
  }

  public boolean resume() {
    if (isRunning() && isPaused()) {
      totalPauseTime = getTime() - pause;
      pause = 0;
      return true;
    }
    return false;
  }

  public boolean stop() {
    if (isRunning()) {
      end = getTime();
      resume();
      return true;
    }
    return false;
  }

  public static class NanosecondStopwatch extends Stopwatch {
    @Override
    public long getTime() {
      return System.nanoTime();
    }
  }

  public static class MicrosecondStopwatch extends Stopwatch {
    @Override
    public long getTime() {
      return System.nanoTime() / 1000;
    }
  }

  public static class MillisecondStopwatch extends Stopwatch {
    @Override
    public long getTime() {
      return System.currentTimeMillis();
    }
  }

  public static class SecondStopwatch extends Stopwatch {
    @Override
    public long getTime() {
      return System.currentTimeMillis() / 1000;
    }
  }

  public static class MinuteStopwatch extends Stopwatch {
    @Override
    public long getTime() {
      return System.currentTimeMillis() / 60000;
    }
  }

  public static Stopwatch getStopwatch(TimeUnit timeUnit) {
    switch (timeUnit) {
      case NANOSECONDS: {
        return new NanosecondStopwatch();
      }
      case MICROSECONDS: {
        return new MicrosecondStopwatch();
      }
      case MILLISECONDS: {
        return new MillisecondStopwatch();
      }
      case SECONDS: {
        return new SecondStopwatch();
      }
      case MINUTES: {
        return new MinuteStopwatch();
      }
      default: {
        return null;
      }
    }
  }

}
