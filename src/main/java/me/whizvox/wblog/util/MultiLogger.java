package me.whizvox.wblog.util;

import org.slf4j.helpers.MarkerIgnoringBase;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

public class MultiLogger extends MarkerIgnoringBase {

  public static final DateFormat FORMAT_DEFAULT = new SimpleDateFormat("yyyy/MM/dd kk:mm:ss");

  private Level baseLevel;
  private Map<String, Consumer<String>> outputs;
  private DateFormat dateFormat;

  public MultiLogger(String name, Level baseLevel, DateFormat dateFormat) {
    this.name = name;
    this.baseLevel = baseLevel;
    this.dateFormat = dateFormat;
    outputs = new HashMap<>();
  }

  public MultiLogger(String name) {
    this(name, Level.INFO, FORMAT_DEFAULT);
  }

  public boolean registerOutput(String name, Consumer<String> output) {
    return outputs.putIfAbsent(name, output) == output;
  }

  public boolean removeOutput(String name) {
    return outputs.remove(name) != null;
  }

  private void postLine(String line) {
    outputs.values().forEach(out -> out.accept(line));
  }

  private void log(Level level, String msg, Object[] args, Throwable t) {
    if (!isLevelEnabled(level)) {
      return;
    }

    StringBuilder sb = new StringBuilder();
    sb.append(dateFormat.format(new Date())).append(" [");
    sb.append(Thread.currentThread().getName()).append("] ");
    sb.append(level.asStr).append(' ');
    sb.append(getName()).append(" - ");
    if (args != null && args.length != 0) {
      sb.append(String.format(msg, args));
    } else {
      sb.append(msg);
    }
    postLine(sb.toString());

    if (t != null) {
      sb.setLength(0);
      sb.append(t.getClass().getName()).append(" - ").append(t.getMessage());
      postLine(sb.toString());
      for (StackTraceElement ste : t.getStackTrace()) {
        sb.setLength(0);
        sb.append("  ").append(ste.toString());
        postLine(sb.toString());
      }
    }
  }

  private boolean isLevelEnabled(Level level) {
    return level.level >= baseLevel.level;
  }

  @Override
  public boolean isTraceEnabled() {
    return isLevelEnabled(Level.TRACE);
  }

  @Override
  public void trace(String msg) {
    log(Level.TRACE, msg, null, null);
  }

  @Override
  public void trace(String format, Object arg) {
    log(Level.TRACE, format, new Object[] {arg}, null);
  }

  @Override
  public void trace(String format, Object arg1, Object arg2) {
    log(Level.TRACE, format, new Object[] {arg1, arg2}, null);
  }

  @Override
  public void trace(String format, Object... arguments) {
    log(Level.TRACE, format, arguments, null);
  }

  @Override
  public void trace(String msg, Throwable t) {
    log(Level.TRACE, msg, null, t);
  }

  @Override
  public boolean isDebugEnabled() {
    return isLevelEnabled(Level.DEBUG);
  }

  @Override
  public void debug(String msg) {
    log(Level.DEBUG, msg, null, null);
  }

  @Override
  public void debug(String format, Object arg) {
    log(Level.DEBUG, format, new Object[] {arg}, null);
  }

  @Override
  public void debug(String format, Object arg1, Object arg2) {
    log(Level.DEBUG, format, new Object[] {arg1, arg2}, null);
  }

  @Override
  public void debug(String format, Object... arguments) {
    log(Level.DEBUG, format, arguments, null);
  }

  @Override
  public void debug(String msg, Throwable t) {
    log(Level.DEBUG, msg, null, t);
  }

  @Override
  public boolean isInfoEnabled() {
    return isLevelEnabled(Level.INFO);
  }

  @Override
  public void info(String msg) {
    log(Level.INFO, msg, null, null);
  }

  @Override
  public void info(String format, Object arg) {
    log(Level.INFO, format, new Object[] {arg}, null);
  }

  @Override
  public void info(String format, Object arg1, Object arg2) {
    log(Level.INFO, format, new Object[] {arg1, arg2}, null);
  }

  @Override
  public void info(String format, Object... arguments) {
    log(Level.INFO, format, arguments, null);
  }

  @Override
  public void info(String msg, Throwable t) {
    log(Level.INFO, msg, null, t);
  }

  @Override
  public boolean isWarnEnabled() {
    return isLevelEnabled(Level.WARN);
  }

  @Override
  public void warn(String msg) {
    log(Level.WARN, msg, null, null);
  }

  @Override
  public void warn(String format, Object arg) {
    log(Level.WARN, format, new Object[] {arg}, null);
  }

  @Override
  public void warn(String format, Object arg1, Object arg2) {
    log(Level.WARN, format, new Object[] {arg1, arg2}, null);
  }

  @Override
  public void warn(String format, Object... arguments) {
    log(Level.WARN, format, arguments, null);
  }

  @Override
  public void warn(String msg, Throwable t) {
    log(Level.WARN, msg, null, t);
  }

  @Override
  public boolean isErrorEnabled() {
    return isLevelEnabled(Level.ERROR);
  }

  @Override
  public void error(String msg) {
    log(Level.ERROR, msg, null, null);
  }

  @Override
  public void error(String format, Object arg) {
    log(Level.ERROR, format, new Object[] {arg}, null);
  }

  @Override
  public void error(String format, Object arg1, Object arg2) {
    log(Level.ERROR, format, new Object[] {arg1, arg2}, null);
  }

  @Override
  public void error(String format, Object... arguments) {
    log(Level.ERROR, format, arguments, null);
  }

  @Override
  public void error(String msg, Throwable t) {
    log(Level.ERROR, msg, null, t);
  }

  public enum Level {
    TRACE(10, "TRACE"),
    DEBUG(20, "DEBUG"),
    INFO (30, "INFO "),
    WARN (40, "WARN "),
    ERROR(50, "ERROR");

    public final int level;
    public final String asStr;
    Level(int level, String asStr) {
      this.level = level;
      this.asStr = asStr;
    }
  }

}
