package me.whizvox.wblog.util;

import org.jooq.Record;
import org.jooq.Result;

import java.util.ArrayList;
import java.util.List;

public abstract class RecordParser<TYPE, RECORD extends Record> {

  public abstract TYPE fromRecord(RECORD record);

  public abstract RECORD fromObject(TYPE object);

  public List<TYPE> fromRecords(Result<RECORD> records) {
    List<TYPE> res = new ArrayList<>();
    records.forEach(record -> res.add(fromRecord(record)));
    return res;
  }

}
