package me.whizvox.wblog.util;

import org.jooq.DSLContext;
import org.jooq.Table;

public class JooqRepository {

  protected DSLContext create;
  private Table table;

  public JooqRepository(DSLContext create, Table table) {
    this.create = create;
    this.table = table;
  }

  public void create() {
    create.createTableIfNotExists(table)
        .columns(table.fields())
        .execute();
  }

  public void drop() {
    create.dropTable(table)
        .execute();
  }

}
