package me.whizvox.wblog.util;

import org.owasp.html.HtmlPolicyBuilder;
import org.owasp.html.PolicyFactory;

public class HtmlUtil {

  private HtmlUtil() {}

  private static final PolicyFactory POLICY_PLAIN_TEXT = new HtmlPolicyBuilder()
      .allowStandardUrlProtocols()
      .requireRelNofollowOnLinks()
      .toFactory();

  private static final PolicyFactory POLICY_HTML = new HtmlPolicyBuilder()
      .allowStandardUrlProtocols()
      .disallowElements("script", "form", "input")
      .requireRelNofollowOnLinks()
      .toFactory();

  public static String sanitizePlainText(String text) {
    return POLICY_PLAIN_TEXT.sanitize(text);
  }

  public static String sanitizeHtml(String text) {
    return POLICY_HTML.sanitize(text);
  }

}
