package me.whizvox.wblog.util;

import org.commonmark.node.Node;
import org.commonmark.parser.Parser;
import org.commonmark.renderer.html.HtmlRenderer;

public class MarkupHelper {

  private static Parser markupParser = Parser.builder().build();
  private static HtmlRenderer htmlRenderer = HtmlRenderer.builder().build();

  public static String parse(String markup) {
    Node node = markupParser.parse(markup);
    return htmlRenderer.render(node);
  }

}
