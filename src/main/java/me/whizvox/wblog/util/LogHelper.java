package me.whizvox.wblog.util;

import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.atomic.AtomicInteger;

public class LogHelper {

  private LogHelper() {}

  private static Path logsDir = null;
  private static boolean initialized = false;
  private static Path logFile = null;
  private static PrintStream logPrintStream;

  public static void initialize(Path baseDir, MultiLogger multiLogger) throws IOException {
    if (initialized) {
      return;
    }
    if (baseDir == null) {
      logsDir = Paths.get("logs");
    } else {
      logsDir = baseDir.resolve("logs");
    }
    IOUtils.mkdirs(logsDir);
    logFile = logsDir.resolve(StringUtils.getFileFriendlyDate(new Date()) + ".log");
    logPrintStream = new PrintStream(logFile.toFile());
    multiLogger.registerOutput("logFile", logPrintStream::println);

    initialized = true;
  }

  public static void initialize(MultiLogger multiLogger) throws IOException {
    initialize(null, multiLogger);
  }

  public static boolean isInitialized() {
    return initialized;
  }

  public static int cleanLogs(int numDays) throws IOException {
    AtomicInteger numLogs = new AtomicInteger(0);
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(new Date());
    calendar.add(Calendar.DATE, -numDays);
    Date expirationDate = calendar.getTime();
    Files.walk(logsDir, 1)
        .filter(path -> StringUtils.getFileExtension(path.getFileName().toString()).equals("log"))
        .filter(path -> {
          String dateStr = StringUtils.getFileNameWithoutExtension(path.getFileName().toString());
          Date date = StringUtils.parseFileFriendlyDate(dateStr);
          if (date != null) {
            return date.before(expirationDate);
          }
          return false;
        })
        .forEach(path -> {
          try {
            Files.delete(path);
            numLogs.addAndGet(1);
          } catch (IOException ignored) {}
        });
    return numLogs.get();
  }

  public static void close() {
    if (initialized) {
      logPrintStream.close();
      logPrintStream = null;
      logFile = null;
      initialized = false;
    }
  }

}
