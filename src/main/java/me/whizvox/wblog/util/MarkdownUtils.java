package me.whizvox.wblog.util;

import org.commonmark.node.Node;
import org.commonmark.parser.Parser;
import org.commonmark.renderer.html.HtmlRenderer;

import java.io.IOException;
import java.io.Reader;

public class MarkdownUtils {

  private static Parser parser = Parser.builder().build();
  private static HtmlRenderer renderer = HtmlRenderer.builder().build();

  public static String render(String input) {
    Node doc = parser.parse(input);
    return renderer.render(doc);
  }

  public static String render(Reader reader) throws IOException {
    Node doc = parser.parseReader(reader);
    return renderer.render(doc);
  }

  public static String render(Node node) {
    return renderer.render(node);
  }

}
