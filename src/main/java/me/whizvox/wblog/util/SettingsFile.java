package me.whizvox.wblog.util;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.*;
import java.util.*;

import static me.whizvox.wblog.Wblog.LOGGER;

public class SettingsFile {

  private List<String> keys;
  private Map<String, String> items;

  public SettingsFile() {
    keys = new ArrayList<>();
    items = new HashMap<>();
  }

  public SettingsFile(final SettingsFile other) {
    this();
    keys.addAll(other.keys);
    items.putAll(other.items);
  }

  public boolean has(String key) {
    return items.containsKey(key);
  }

  public String get(String key, String defaultValue) {
    if (!items.containsKey(key)) {
      set(key, defaultValue);
      return defaultValue;
    }
    return items.get(key);
  }

  public String get(String key) {
    return items.get(key);
  }

  public <TYPE> TYPE get(String key, TYPE defaultValue, ValueParser<TYPE> parser) {
    if (items.containsKey(key)) {
      try {
        return parser.parse(get(key));
      } catch (Exception ignored) {}
    }
    set(key, parser.toString(defaultValue));
    return defaultValue;
  }

  public boolean getBoolean(String key, boolean defaultValue) {
    return get(key, defaultValue, StringUtils::parseBoolean);
  }

  public char getChar(String key, char defaultValue) {
    return get(key, defaultValue, StringUtils::parseChar);
  }

  public short getShort(String key, short defaultValue) {
    return get(key, defaultValue, Short::parseShort);
  }

  public int getInteger(String key, int defaultValue) {
    return get(key, defaultValue, Integer::parseInt);
  }

  public long getLong(String key, long defaultValue) {
    return get(key, defaultValue, Long::parseLong);
  }

  public float getFloat(String key, float defaultValue) {
    return get(key, defaultValue, Float::parseFloat);
  }

  public double getDouble(String key, double defaultValue) {
    return get(key, defaultValue, Double::parseDouble);
  }

  public Date getDate(String key, Date defaultValue) {
    return get(key, defaultValue, StringUtils::parseDate);
  }

  public boolean set(String key, Object value) {
    if (key != null && !key.isEmpty() && !StringUtils.containsChar(key, '=') && !StringUtils.containsChar(key, ' ')) {
      if (value != null) {
        items.put(key, String.valueOf(value));
      } else {
        items.put(key, null);
      }
      keys.add(key);
      return true;
    }
    return false;
  }

  public void load(BufferedReader reader) throws IOException {
    items.clear();
    String line;
    List<String> tokens = new ArrayList<>();
    int n = 1;
    while ((line = reader.readLine()) != null) {
      line = line.trim();
      if (!line.isEmpty() && line.charAt(0) != '#') {
        tokens.clear();
        StringUtils.tokenize(line, '=', tokens, 2);
        if (tokens.size() == 2) {
          String key = tokens.get(0).trim();
          String value = tokens.get(1).trim();
          if (!set(key, value)) {
            LOGGER.warn("Bad line found in config file <lineNumber=%d, line=%s>", n, line);
          }
        } else {
          LOGGER.warn("Bad line found in config file <lineNumber=%d, line=%s>", n, line);
        }
      }
      n++;
    }
  }

  public void load(ObjectMapper objectMapper, InputStream in) throws IOException {
    items.clear();
    Map<String, String> newItems = objectMapper.readValue(in,
        objectMapper.getTypeFactory().constructMapType(HashMap.class, String.class, String.class));
    items.putAll(newItems);
  }

  public void save(BufferedWriter writer) throws IOException {
    for (String key : keys) {
      final String value = items.get(key);
      if (value != null) {
        writer.write(key);
        writer.write('=');
        writer.write(value);
        writer.newLine();
      }
    }
  }

  public void save(ObjectMapper objectMapper, OutputStream out) throws IOException {
    objectMapper.writeValue(out, items);
  }

  public interface ValueParser<TYPE> {
    TYPE parse(String str) throws Exception;
    default String toString(TYPE type) {
      return String.valueOf(type);
    }
  }

}
