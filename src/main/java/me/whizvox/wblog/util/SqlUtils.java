package me.whizvox.wblog.util;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class SqlUtils {

  public static Timestamp getTimestampFromDate(Date date) {
    return Timestamp.from(date.toInstant());
  }

  public static Timestamp getCurrentTimestamp() {
    return new Timestamp(System.currentTimeMillis());
  }

  /**
   * Setups part of a SQL statement to be implemented in a search query.
   * <p>
   * General idea behind this: say the user inputs the following query:
   *    <pre>item1 item2 "item 3"</pre>
   * The query will be split up into a list of ["item1", "item2", "item 3"]
   * and the server wants to search through the following fields:
   *    <pre>title, contents, tags</pre>
   * The eventual resulting query will look like this:
   *    <pre>SELECT * FROM something WHERE
   *      title LIKE "item1" OR contents LIKE "item1" OR tags LIKE "item1" OR
   *      title LIKE "item2" OR contents LIKE "item2" OR tags LIKE "item2" OR
   *      title LIKE "item 3" OR contents LIKE "item 3" OR tags LIKE "item 3";</pre>
   * The number of conditions can be represented by the following equation:
   *    <pre>conditionCount = fieldCount * itemCount</pre>
   * so it's best to not have too many items in your search query or too many fields to search.
   * </p>
   * @param itemsCount The number of items the statement has to account for.
   * @param firstField The name of the first field to search through.
   * @param otherFields The names of the other fields to search through.
   * @return A prepared SQL string with the condition to include in the WHERE part of the SQL statement.
   * <p>Example (if firstFirld = "field1" and otherFields = ["field2"] and itemsCount = 2):
   *  <pre>field1 LIKE ? OR field1 LIKE ? OR field2 LIKE ? OR field2 LIKE ?</pre>
   * </p>
   * @see #prepareLikeQueryItems(List, int)
   */
  public static String prepareSearchStatement(int itemsCount, String firstField, String... otherFields) {
    if (itemsCount < 1) {
      return null;
    }
    StringBuilder sb = new StringBuilder();
    sb.append(firstField).append(" LIKE ?");
    for (String field : otherFields) {
      sb.append(" OR ").append(field).append(" LIKE ?");
    }
    String first = sb.toString();
    String condition = first;
    if (itemsCount > 1) {
      condition = " OR " + first;
    }
    return condition;
  }

  /**
   * Prepares a list of query items to be used with the SQL LIKE conditional.
   * <p>
   *   Basically, it takes every item and adds '%' to the front and back. Also, if the number of fields is 2 or more,
   *   then the items are duplicated.
   * </p>
   * @param queryItems The query items that are to be included in the eventual SQL statement.
   * @param fieldCount The number of fields that need to be accounted for in the duplication step.
   * @return A list of items that are ready to be used in a SQL LIKE condition.
   * <p>
   *   Example (if queryItems = ["item1", "item2"] and fieldCount = 2)
   *   <pre>["%item1%", "%item1%", "%item2%", "%item2%"]</pre>
   * </p>
   * @see #prepareSearchStatement(int, String, String...)
   */
  public static List<String> prepareLikeQueryItems(List<String> queryItems, int fieldCount) {
    if (queryItems == null || queryItems.isEmpty() || fieldCount < 1) {
      return Collections.emptyList();
    }
    List<String> items = new ArrayList<>(queryItems);
    for (int i = 0; i < items.size(); i++) {
      items.set(i, "%" + items.get(i) + "%");
    }
    if (fieldCount > 1) {
      List<String> newItems = new ArrayList<>(items.size() * fieldCount);
      for (String item : items) {
        for (int i = 0; i < fieldCount; i++) {
          newItems.add(item);
        }
      }
      items = newItems;
    }
    return items;
  }

}
