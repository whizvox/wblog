package me.whizvox.wblog.util;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Comparator;

public class IOUtils {

  public static URL getInternalResource(String path) {
    return IOUtils.class.getClassLoader().getResource(path);
  }

  public static boolean copyFromResource(String internalPath, Path rootExternalPath) throws IOException {
    Path target = rootExternalPath.resolve(Paths.get(internalPath));
    if (!Files.exists(target)) {
      URL url = getInternalResource(internalPath);
      if (url == null) {
        return false;
      }
      try (InputStream in = url.openStream()) {
        Files.copy(in, target);
      }
      return true;
    }
    return false;
  }

  public static boolean mkdirs(Path path) {
    if (!Files.exists(path)) {
      try {
        Files.createDirectories(path);
      } catch (IOException ignored) {
        return false;
      }
    }
    return true;
  }

  public static boolean touch(Path path) {
    if (!Files.exists(path)) {
      try {
        Files.createFile(path);
        return true;
      } catch (IOException ignored) {}
    }
    return false;
  }

  public static boolean deleteRecursive(Path p) throws IOException {
    Files.walk(p)
        .sorted(Comparator.reverseOrder())
        .map(Path::toFile)
        .forEach(File::delete);
    return Files.notExists(p);
  }

  public static Path getEndSubpath(Path path, int from) {
    final int L = path.getNameCount();
    return path.subpath(L - from, L);
  }

}
