package me.whizvox.wblog.route;

import me.whizvox.wblog.Wblog;
import spark.Request;
import spark.Response;

import java.util.Map;

public class HtmlPageRoute extends WblogRoute {

  private String templateName;

  public HtmlPageRoute(Wblog wblog, String templateName) {
    super(wblog);
    this.templateName = templateName;
  }

  @Override
  public Object handle(Request request, Response response) throws Exception {
    return null;
  }

  /*protected Object handle_do(Request request, Response response) {

  }*/

}
