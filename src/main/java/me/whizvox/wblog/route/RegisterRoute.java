package me.whizvox.wblog.route;

import me.whizvox.wblog.Wblog;
import me.whizvox.wblog.i18n.Localizer;
import me.whizvox.wblog.page.HtmlPage;
import me.whizvox.wblog.page.NavigationEntry;
import me.whizvox.wblog.page.PageHelper;
import spark.Request;
import spark.Response;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RegisterRoute extends WblogRoute {

  public RegisterRoute(Wblog wblog) {
    super(wblog);
  }

  @Override
  public Object handle(Request request, Response response) throws Exception {
    Map<String, Object> dm = new HashMap<>();
    HtmlPage page = PageHelper.setupHtmlPage();
    // TODO: Remove temp code
    page.title = "Register (TEMP)";
    page.language = "en";
    List<NavigationEntry> navpages = wblog.getNavigationManager().getNavigation(page.language);
    dm.put("htmlPage", page);
    dm.put("hnav", navpages);
    dm.put("_", new Localizer.LocalizeFreemarkerMethod(wblog.getLocalizer()));
    return wblog.getPageService().getTemplateManager().process("register", dm);
  }

}
