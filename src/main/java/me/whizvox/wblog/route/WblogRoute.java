package me.whizvox.wblog.route;

import me.whizvox.wblog.Wblog;
import spark.Route;

public abstract class WblogRoute implements Route {

  protected Wblog wblog;

  public WblogRoute(Wblog wblog) {
    this.wblog = wblog;
  }

}
