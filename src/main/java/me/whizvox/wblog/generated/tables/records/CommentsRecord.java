/*
 * This file is generated by jOOQ.
 */
package me.whizvox.wblog.generated.tables.records;


import java.sql.Timestamp;

import javax.annotation.Generated;

import me.whizvox.wblog.generated.tables.Comments;

import org.jooq.Field;
import org.jooq.Record1;
import org.jooq.Record8;
import org.jooq.Row8;
import org.jooq.impl.UpdatableRecordImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.11.5"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class CommentsRecord extends UpdatableRecordImpl<CommentsRecord> implements Record8<Long, Long, Long, String, Timestamp, Timestamp, Long, Integer> {

    private static final long serialVersionUID = -1660956487;

    /**
     * Setter for <code>comments.id</code>.
     */
    public void setId(Long value) {
        set(0, value);
    }

    /**
     * Getter for <code>comments.id</code>.
     */
    public Long getId() {
        return (Long) get(0);
    }

    /**
     * Setter for <code>comments.blog_post_id</code>.
     */
    public void setBlogPostId(Long value) {
        set(1, value);
    }

    /**
     * Getter for <code>comments.blog_post_id</code>.
     */
    public Long getBlogPostId() {
        return (Long) get(1);
    }

    /**
     * Setter for <code>comments.author_id</code>.
     */
    public void setAuthorId(Long value) {
        set(2, value);
    }

    /**
     * Getter for <code>comments.author_id</code>.
     */
    public Long getAuthorId() {
        return (Long) get(2);
    }

    /**
     * Setter for <code>comments.contents</code>.
     */
    public void setContents(String value) {
        set(3, value);
    }

    /**
     * Getter for <code>comments.contents</code>.
     */
    public String getContents() {
        return (String) get(3);
    }

    /**
     * Setter for <code>comments.published</code>.
     */
    public void setPublished(Timestamp value) {
        set(4, value);
    }

    /**
     * Getter for <code>comments.published</code>.
     */
    public Timestamp getPublished() {
        return (Timestamp) get(4);
    }

    /**
     * Setter for <code>comments.last_edited</code>.
     */
    public void setLastEdited(Timestamp value) {
        set(5, value);
    }

    /**
     * Getter for <code>comments.last_edited</code>.
     */
    public Timestamp getLastEdited() {
        return (Timestamp) get(5);
    }

    /**
     * Setter for <code>comments.parent_comment_id</code>.
     */
    public void setParentCommentId(Long value) {
        set(6, value);
    }

    /**
     * Getter for <code>comments.parent_comment_id</code>.
     */
    public Long getParentCommentId() {
        return (Long) get(6);
    }

    /**
     * Setter for <code>comments.score</code>.
     */
    public void setScore(Integer value) {
        set(7, value);
    }

    /**
     * Getter for <code>comments.score</code>.
     */
    public Integer getScore() {
        return (Integer) get(7);
    }

    // -------------------------------------------------------------------------
    // Primary key information
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public Record1<Long> key() {
        return (Record1) super.key();
    }

    // -------------------------------------------------------------------------
    // Record8 type implementation
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public Row8<Long, Long, Long, String, Timestamp, Timestamp, Long, Integer> fieldsRow() {
        return (Row8) super.fieldsRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Row8<Long, Long, Long, String, Timestamp, Timestamp, Long, Integer> valuesRow() {
        return (Row8) super.valuesRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Long> field1() {
        return Comments.COMMENTS.ID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Long> field2() {
        return Comments.COMMENTS.BLOG_POST_ID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Long> field3() {
        return Comments.COMMENTS.AUTHOR_ID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field4() {
        return Comments.COMMENTS.CONTENTS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Timestamp> field5() {
        return Comments.COMMENTS.PUBLISHED;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Timestamp> field6() {
        return Comments.COMMENTS.LAST_EDITED;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Long> field7() {
        return Comments.COMMENTS.PARENT_COMMENT_ID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Integer> field8() {
        return Comments.COMMENTS.SCORE;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Long component1() {
        return getId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Long component2() {
        return getBlogPostId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Long component3() {
        return getAuthorId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String component4() {
        return getContents();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Timestamp component5() {
        return getPublished();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Timestamp component6() {
        return getLastEdited();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Long component7() {
        return getParentCommentId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer component8() {
        return getScore();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Long value1() {
        return getId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Long value2() {
        return getBlogPostId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Long value3() {
        return getAuthorId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value4() {
        return getContents();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Timestamp value5() {
        return getPublished();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Timestamp value6() {
        return getLastEdited();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Long value7() {
        return getParentCommentId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer value8() {
        return getScore();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CommentsRecord value1(Long value) {
        setId(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CommentsRecord value2(Long value) {
        setBlogPostId(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CommentsRecord value3(Long value) {
        setAuthorId(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CommentsRecord value4(String value) {
        setContents(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CommentsRecord value5(Timestamp value) {
        setPublished(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CommentsRecord value6(Timestamp value) {
        setLastEdited(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CommentsRecord value7(Long value) {
        setParentCommentId(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CommentsRecord value8(Integer value) {
        setScore(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CommentsRecord values(Long value1, Long value2, Long value3, String value4, Timestamp value5, Timestamp value6, Long value7, Integer value8) {
        value1(value1);
        value2(value2);
        value3(value3);
        value4(value4);
        value5(value5);
        value6(value6);
        value7(value7);
        value8(value8);
        return this;
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached CommentsRecord
     */
    public CommentsRecord() {
        super(Comments.COMMENTS);
    }

    /**
     * Create a detached, initialised CommentsRecord
     */
    public CommentsRecord(Long id, Long blogPostId, Long authorId, String contents, Timestamp published, Timestamp lastEdited, Long parentCommentId, Integer score) {
        super(Comments.COMMENTS);

        set(0, id);
        set(1, blogPostId);
        set(2, authorId);
        set(3, contents);
        set(4, published);
        set(5, lastEdited);
        set(6, parentCommentId);
        set(7, score);
    }
}
