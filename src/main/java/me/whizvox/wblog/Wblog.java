package me.whizvox.wblog;

import com.fasterxml.jackson.databind.ObjectMapper;
import freemarker.template.Configuration;
import me.whizvox.wblog.auth.AuthenticationService;
import me.whizvox.wblog.event.EventManager;
import me.whizvox.wblog.event.ServerEvent;
import me.whizvox.wblog.event.ServerEventHandler;
import me.whizvox.wblog.i18n.Language;
import me.whizvox.wblog.i18n.LanguagesList;
import me.whizvox.wblog.i18n.Localizer;
import me.whizvox.wblog.page.*;
import me.whizvox.wblog.permission.GroupManager;
import me.whizvox.wblog.route.RegisterPostRoute;
import me.whizvox.wblog.route.RegisterRoute;
import me.whizvox.wblog.util.*;
import me.whizvox.wblog.version.UpdateChecker;
import org.jooq.DSLContext;
import org.jooq.impl.DSL;
import org.slf4j.Logger;
import spark.Spark;

import java.io.*;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static spark.Spark.*;

public class Wblog implements AutoCloseable, Runnable {

  private final Path baseDir;
  private SettingsFile settings;
  private ObjectMapper objectMapper;
  private UpdateChecker updateChecker;
  private EventManager eventManager;
  private Localizer localizer;
  private GroupManager groupManager;
  private NavigationManager navigationManager;

  private ScheduledExecutorService scheduledExecutorService;

  private AuthenticationService authService;
  private PageService pageService;
  private DSLContext dslContext;

  private boolean initialized;
  private boolean shuttingDown;
  private boolean restarting;
  private boolean continuouslyRestarting;

  public Wblog(String baseDir) {
    initialized = false;
    shuttingDown = false;
    restarting = false;
    continuouslyRestarting = false;
    this.baseDir = Paths.get(baseDir);
  }

  public Wblog() {
    this(".");
  }

  public boolean isInitialized() {
    return initialized;
  }

  public SettingsFile getSettings() {
    return settings;
  }

  public EventManager getEventManager() {
    return eventManager;
  }

  public ObjectMapper getObjectMapper() {
    return objectMapper;
  }

  public AuthenticationService getAuthenticationService() {
    return authService;
  }

  public PageService getPageService() {
    return pageService;
  }

  public Localizer getLocalizer() {
    return localizer;
  }

  public NavigationManager getNavigationManager() {
    return navigationManager;
  }

  // depends on nothing
  // should be the first thing initialized
  private void initLogger() throws IOException {
    if (LogHelper.isInitialized()) {
      LOGGER.warn("File logging is already active. Won't do anything else.");
    } else {
      LOGGER.info("Initializing file logging...");
      LogHelper.initialize(baseDir, (MultiLogger) LOGGER);
      LOGGER.info("File logging active. Now cleaning up old logs...");
      int n = LogHelper.cleanLogs(14);
      LOGGER.info("Successfully deleted %d old log(s).", n);
    }
  }

  // depends on nothing
  private void initSettings() throws IOException {
    LOGGER.info("Initializing and reading from settings...");
    settings = new SettingsFile();
    Path settingsFilePath = baseDir.resolve(Reference.FilePaths.SETTINGS);
    if (IOUtils.copyFromResource(Reference.FilePaths.SETTINGS, baseDir)) {
      LOGGER.info("Copied settings file from internal resource");
    }
    try (BufferedReader reader = Files.newBufferedReader(settingsFilePath)) {
      settings.load(reader);
    }
  }

  // depends on #initSettings()
  private void initDslContext() throws SQLException {
    LOGGER.info("Initializing DSL context...");
    String jdbcUrl = settings.get(Reference.SettingKeys.JDBC_URL, "jdbc:sqlite:${BASE}/wblog.db");
    String jdbcUrlRepl = jdbcUrl.replace("${BASE}", baseDir.toString());
    if (!jdbcUrl.equals(jdbcUrlRepl)) {
      LOGGER.info("JDBC URL found to contain ${BASE}. Resolved URL is now %s", jdbcUrlRepl);
    }
    String jdbcUsername = settings.get(Reference.SettingKeys.JDBC_USERNAME, null);
    String jdbcPassword = settings.get(Reference.SettingKeys.JDBC_PASSWORD, null);
    dslContext = DSL.using(DriverManager.getConnection(jdbcUrlRepl, jdbcUsername, jdbcPassword));
  }

  // depends on #initSettings()
  private void initUpdateChecker() {
    if (Reference.isDevVersion()) {
      LOGGER.warn("Will not check for updates since this is a dev build.");
      return;
    }
    LOGGER.info("Initializing update checker...");
    boolean checkUpdates = settings.getBoolean(Reference.SettingKeys.CHECK_UPDATES, true);
    try {
      updateChecker = new UpdateChecker(objectMapper, Reference.UPDATE_CHECK_URL);
    } catch (MalformedURLException e) {
      LOGGER.error("Failed to initialize update checker: Invalid url <%s>" + Reference.UPDATE_CHECK_URL);
      checkUpdates = false;
    }
    if (checkUpdates) {
      LOGGER.info("Checking for updates...");
      try {
        UpdateChecker.Result r = checkForUpdates();
        switch (r.type) {
          case SUCCESS: {
            UpdateChecker.Status status = updateStatus(r);
            switch (status) {
              case DEV_BUILD: {
                LOGGER.warn("YOU ARE CURRENTLY RUNNING AN UNDOCUMENTED DEVELOPMENT BUILD. USE AT YOUR OWN RISK!");
                break;
              }
              case INDETERMINATE: {
                LOGGER.warn("Could not determine update status.");
                break;
              }
              case UP_TO_DATE: {
                LOGGER.info("No updates available.");
                break;
              }
              case UPDATE_AVAILABLE: {
                LOGGER.info("There's a new update available <%s>, released <%s>", r.version, StringUtils.getHumanFriendlyDate(r.when));
                break;
              }
            }
            break;
          }
          case INCOMPLETE: {
            LOGGER.error("Could not complete update check as JSON source is incomplete");
            break;
          }
          case MALFORMED: {
            LOGGER.error("Could not complete update check as JSON source is malformed");
            break;
          }
        }
      } catch (IOException e) {
        LOGGER.error("Could not check for updates.", e);
      }
    } else {
      LOGGER.warn("Update checking is disabled. This is not recommended.");
    }
  }

  // depends on nothing
  private void initObjectMapper() {
    LOGGER.info("Initializing object mapper...");
    objectMapper = new ObjectMapper();
  }

  // depends on nothing
  private void initScheduledExecutorService() {
    LOGGER.info("Initializing executor service...");
    scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();
  }

  // depends on nothing
  private void initEventManager() {
    LOGGER.info("Initializing event manager and registering server event handlers...");
    eventManager = new EventManager();
    eventManager.registerHandler(ServerEventHandler.INSTANCE, ServerEvent.class);
  }

  // depends on nothing
  private void initLocalizer() throws IOException {
    LOGGER.info("Initializing localizer...");
    Path langsRootPath = baseDir.resolve(Reference.FilePaths.LANG_DIR);
    Path langsList = baseDir.resolve(Reference.FilePaths.LANGUAGES_LIST);

    IOUtils.mkdirs(langsRootPath);
    if (IOUtils.copyFromResource(Reference.FilePaths.LANGUAGES_LIST, baseDir)) {
      LOGGER.info("Languages list copied from internal resource.");
    }

    List<String> langKeys = new ArrayList<>();
    String fallbackLang = Reference.DEFAULT_FALLBACK_LANGUAGE;
    if (!Files.exists(langsList)) {
      LOGGER.error("Languages list <%s> was not found. Will use fallback instead.", Reference.FilePaths.LANGUAGES_LIST);
      langKeys.add("en");
    } else {
      LanguagesList list = objectMapper.readValue(langsList.toFile(), LanguagesList.class);
      if (list == null || list.languages.length == 0) {
        LOGGER.error("Reading from languages.json yielded no languages. Will use fallback instead.");
        langKeys.add("en");
      } else {
        Collections.addAll(langKeys, list.languages);
        fallbackLang = list.fallback;
      }
    }
    Map<String, Language> languages = new HashMap<>();
    langKeys.forEach(key -> {
      String fileName = key + ".lang";
      Path path = langsRootPath.resolve(fileName);
      try {
        if (IOUtils.copyFromResource(Reference.FilePaths.LANG_DIR + "/" + fileName, baseDir)) {
          LOGGER.info("Copied lang file <%s>", path);
        }
        languages.put(key, Language.parse(path.toUri().toURL()));
        LOGGER.info("Language <%s> successfully parsed", key);
      } catch (IOException e) {
        LOGGER.error(String.format("Could not read from lang file <%s>", path.toString()), e);
      }
    });
    localizer = new Localizer(fallbackLang);
    languages.forEach((k, v) -> localizer.registerNewLanguage(k, v));
  }

  // depends on #initDslContext()
  private void initAuthenticationService() {
    LOGGER.info("Building authentication service...");
    authService = AuthenticationService.newBuilder()
        .setDslContext(dslContext)
        .build();
    LOGGER.info("Initializing authentication service repositories...");
    authService.createRepositories();
  }

  // depends on #initDslContext() and #initLocalizer()
  private void initPageService() {
    LOGGER.info("Configuring page service's template loader...");
    Configuration freemarkerConfig = new Configuration(Configuration.VERSION_2_3_26);
    AutoCopyTemplateLoader templateLoader = new AutoCopyTemplateLoader(Wblog.class, "templates", baseDir);
    freemarkerConfig.setTemplateLoader(templateLoader);
    freemarkerConfig.setLocalizedLookup(false);

    LOGGER.info("Building page service...");
    pageService = PageService.builder()
        .setDslContext(dslContext)
        .setLocalizer(localizer)
        .setTemplateManager(new TemplateManager(freemarkerConfig))
        .build();
    LOGGER.info("Initializing page service repositories...");
    pageService.createRepositories();
  }

  // depends on #initPageService()
  private void initNavigationManager() throws IOException {
    LOGGER.info("Initializing navigation manager...");
    navigationManager = new NavigationManager(pageService);
    Path navigationFilePath = baseDir.resolve(Reference.FilePaths.NAVIGATION);
    if (IOUtils.copyFromResource(Reference.FilePaths.NAVIGATION, baseDir)) {
      LOGGER.info("Navigation file copied from internal resource.");
    }
    try (InputStream in = Files.newInputStream(navigationFilePath)) {
      navigationManager.load(objectMapper, in);
    }
  }

  // depends on #initObjectMapper()
  private void initGroups() throws IOException {
    LOGGER.info("Initializing group permissions...");
    groupManager = new GroupManager();
    if (IOUtils.copyFromResource(Reference.FilePaths.GROUPS, baseDir)) {
      LOGGER.info("Groups file copied from internal resource.");
    }
    try (InputStream in = Files.newInputStream(baseDir.resolve(Reference.FilePaths.GROUPS))) {
      groupManager.load(objectMapper, in);
    }
  }

  // depends on #initSettings()
  // should also be the last thing to initialize
  private void initSpark() throws IOException {
    LOGGER.info("Configuring Spark...");
    String staticFilesLocation = settings.get(Reference.SettingKeys.SPARK_STATIC_FILES_LOCATION, "static");
    LOGGER.info("Setting static files location <%s>...", staticFilesLocation);
    Path staticDir = baseDir.resolve(staticFilesLocation);
    if (!Files.exists(staticDir)) {
      Files.createDirectory(staticDir);
    }
    externalStaticFileLocation(staticDir.toString());
    int port = settings.getInteger(Reference.SettingKeys.SPARK_PORT, 4568);
    LOGGER.info("Setting port <%s>...", port);
    port(port);

    LOGGER.info("Initializing Spark instance...");
    Spark.init();
    awaitInitialization();
  }

  private void postInitSettings() throws IOException {
    LOGGER.info("Saving settings...");
    try (BufferedWriter w = Files.newBufferedWriter(baseDir.resolve(Reference.FilePaths.SETTINGS))) {
      settings.save(w);
    }
    LOGGER.info("Saving navigation...");
    try (OutputStream out = Files.newOutputStream(baseDir.resolve(Reference.FilePaths.NAVIGATION))) {
      navigationManager.save(objectMapper, out);
    }
  }

  private void stopSpark() {
    LOGGER.info("Stopping Spark instance...");
    Spark.stop();
    awaitStop();
  }

  private void stopScheduledExecutorService() {
    scheduledExecutorService.shutdown();
    try {
      scheduledExecutorService.awaitTermination(5, TimeUnit.SECONDS);
    } catch (InterruptedException e) {
      LOGGER.error("Could not successfully shutdown shutdown executor service", e);
    }
  }

  public void init() throws Exception {
    if (isInitialized()) {
      LOGGER.warn("Cannot initialize Wblog as it is already initialized.");
      return;
    }

    Stopwatch stopwatch = Stopwatch.getStopwatch(TimeUnit.MILLISECONDS);
    if (stopwatch != null) {
      stopwatch.start();
    } else {
      LOGGER.error("Could not start stopwatch to time initialization. THIS SHOULDN'T HAPPEN");
    }

    if (!Files.exists(baseDir)) {
      Files.createDirectories(baseDir);
    }

    initLogger();

    if (Reference.isDevVersion()) {
      LOGGER.warn("You are using a development build of Wblog. Use at your own risk!");
    } else {
      LOGGER.info("Running Wblog version %s, released %s", Reference.VERSION, Reference.RELEASED);
    }

    LOGGER.info("Initializing Wblog instance...");

    // depends on nothing
    initObjectMapper();
    initScheduledExecutorService();
    initEventManager();
    initLocalizer();

    // depends on #initSettings()
    initSettings();
    initGroups(); // depends on #initObjectMapper()
    initUpdateChecker();
    initDslContext();

    // depends on #initDslContext()
    initAuthenticationService();
    initPageService();

    // depends on #initPageService() and #initLocalizer()
    initNavigationManager();

    // saved for last
    initSpark();

    if (!continuouslyRestarting) {
      boolean contRestart = settings.getBoolean(Reference.SettingKeys.SERVER_RESTART_AUTO, true);
      if (!contRestart) {
        LOGGER.warn("Wblog has NOT been scheduled to continuously restart itself.");
      } else {
        // TODO: Make the timing for restarting revolve around midnight rather than when the server first starts
        int hours = settings.getInteger(Reference.SettingKeys.SERVER_RESTART_INTERVAL, 24);
        scheduleContinuousRestart(hours);
      }
    }

    before("*", new SetupLanguageFilter(this));
    before("*", new LoginUserFilter(this));

    //get("/:page", new PageRoute(this));

    get("/register", new RegisterRoute(this));
    post("/register", new RegisterPostRoute(this));

    exception(Exception.class, (exception, request, response) -> {
      response.body(StringUtils.exceptionToString(exception));
      LOGGER.error("A fatal exception has kept the request from being completed", exception);
    });
    String landingPage = settings.get(Reference.SettingKeys.SITE_LANDING_PAGE, "home");
    redirect.get("/", "/" + landingPage);

    if (/*settings.getBoolean("DEBUG", false)*/ true) {
      LOGGER.info("Doing debug-only stuff now...");
      get("/debug/:action", (req, res) -> {
        String action = req.params().getOrDefault(":action", null);
        ServerEvent event = new ServerEvent(Wblog.this);
        event.userId = -1;
        if ("shutdown".equals(action)) {
          event.type = ServerEvent.TYPE_SHUTDOWN;
        } else if ("restart".equals(action)) {
          event.type = ServerEvent.TYPE_RESTART;
        } else if ("clearCache".equals(action)) {
          pageService.clearCache();
          return "Page cache cleared";
        } else {
          return "Invalid action: " + action;
        }
        int delay = 5;
        if (req.queryMap().hasKey("delay")) {
          try {
            delay = req.queryMap("delay").integerValue();
          } catch (NumberFormatException ignored) {}
        }
        event.seconds = delay;
        eventManager.postEvent(event);
        return "Server will " + action + " in " + delay + " seconds.";
      });
    }

    postInitSettings();

    initialized = true;
    if (stopwatch != null) {
      stopwatch.stop();
      LOGGER.info("Wblog instance initialized in %d milliseconds.", stopwatch.getElapsedTime());
    }
  }

  private void shutdown() {
    if (!isInitialized()) {
      LOGGER.warn("Cannot shutdown Wblog as it is not currently running.");
      return;
    }

    if (!shuttingDown) {
      LOGGER.error("Cannot shutdown Wblog as a shutdown event has not been scheduled.");
      return;
    }

    LOGGER.info("Shutting down Wblog...");
    Stopwatch stopwatch = Stopwatch.getStopwatch(TimeUnit.MILLISECONDS);
    if (stopwatch != null) {
      stopwatch.start();
    } else {
      LOGGER.error("Could not start stopwatch for shutdown time. THIS SHOULDN'T HAPPEN");
    }

    stopSpark();
    stopScheduledExecutorService();
    closeAndSaveResources();

    continuouslyRestarting = false;
    shuttingDown = false;
    initialized = false;

    if (stopwatch != null) {
      stopwatch.stop();
      LOGGER.info("Wblog successfully shut down in %d milliseconds.", stopwatch.getElapsedTime());
    }
  }

  private void closeAndSaveResources() {
    LOGGER.info("Closing jOOQ DSL context...");
    if (dslContext != null) {
      dslContext.close();
    }
    LOGGER.info("Saving settings...");
    try (BufferedWriter writer = Files.newBufferedWriter(baseDir.resolve(Reference.FilePaths.SETTINGS))) {
      settings.save(writer);
    } catch (IOException e) {
      LOGGER.error("Could not save settings.", e);
    }

    LOGGER.info("Saving group manager permissions...");
    try (OutputStream out = Files.newOutputStream(baseDir.resolve(Reference.FilePaths.GROUPS))) {
      groupManager.save(objectMapper, out);
    } catch (IOException e) {
      LOGGER.error("Could not save group manager permissions.", e);
    }

    LOGGER.info("Saving navigation settings...");
    try (OutputStream out = Files.newOutputStream(baseDir.resolve(Reference.FilePaths.NAVIGATION))) {
      navigationManager.save(objectMapper, out);
    } catch (IOException e) {
      LOGGER.error("Could not save navigation settings.", e);
    }
  }

  public UpdateChecker.Result checkForUpdates() throws IOException {
    return updateChecker.check();
  }

  public UpdateChecker.Status updateStatus(UpdateChecker.Result result) {
    if (Reference.VERSION == null || result.type != UpdateChecker.Result.Type.SUCCESS || result.version == null) {
      return UpdateChecker.Status.INDETERMINATE;
    }
    int cmp = Reference.VERSION.compareTo(result.version);
    if (cmp > 0) {
      return UpdateChecker.Status.DEV_BUILD;
    } else if (cmp == 0) {
      return UpdateChecker.Status.UP_TO_DATE;
    }
    return UpdateChecker.Status.UPDATE_AVAILABLE;
  }

  private void restart() {
    if (!isInitialized()) {
      LOGGER.warn("Cannot restart Wblog as it is not initialized.");
      return;
    }
    if (!restarting) {
      LOGGER.warn("Cannot restart Wblog as a restart event has not been properly scheduled.");
    }
    LOGGER.info("Restarting Wblog!");
    shuttingDown = true;
    shutdown();
    try {
      init();
    } catch (Exception e) {
      LOGGER.error("Could not successfully restart Wblog!", e);
    }
    LOGGER.info("Running system garbage collector...");
    System.gc();
    shuttingDown = false;
    restarting = false;
    LOGGER.info("Restart successful.");
  }

  @Override
  public void run() {
    try {
      while (initialized) {
        if (shuttingDown) {
          shutdown();
        } else if (restarting) {
          restart();
        } else {
          eventManager.loop();
          Thread.sleep(1000);
        }
      }
    } catch (InterruptedException e) {
      LOGGER.error("Main thread interrupted while managing event manager.", e);
    }
  }

  @Override
  public void close() throws Exception {
    closeAndSaveResources();
  }

  public void scheduleShutdown(int seconds) {
    if (shuttingDown) {
      LOGGER.error("Attempted to re-schedule server shutdown.");
    } else {
      LOGGER.warn("Wblog scheduled to SHUTDOWN in %d seconds...", seconds);
      scheduledExecutorService.schedule(() -> {
        shuttingDown = true;
      }, seconds, TimeUnit.SECONDS);
    }
  }

  public void scheduleRestart(int seconds) {
    if (restarting) {
      LOGGER.error("Attempted to re-schedule server restart.");
    } else {
      LOGGER.warn("Wblog scheduled to RESTART in %d seconds...", seconds);
      scheduledExecutorService.schedule(() -> {
        restarting = true;
      }, seconds, TimeUnit.SECONDS);
    }
  }

  public void scheduleContinuousRestart(int hours) {
    if (continuouslyRestarting) {
      LOGGER.error("Attempted to re-schedule continuous server restart.");
    } else {
      if (hours < 4 || hours > 24 * 7) {
        LOGGER.error("Continuous restart has NOT been scheduled. Number of hours <%d> exceeds acceptable range (4 hours, 7 days)", hours);
      } else {
        LOGGER.warn("Wblog scheduled to CONTINUOUSLY RESTART for every %d hours.", hours);
        scheduledExecutorService.scheduleAtFixedRate(() -> restarting = true, hours, hours, TimeUnit.HOURS);
        continuouslyRestarting = true;
      }
    }
  }

  public static final Logger LOGGER;

  static {
    LOGGER = new MultiLogger("Wblog");
    ((MultiLogger) LOGGER).registerOutput("System.out", System.out::println);
    LOGGER.info("Static multi-logger initialized.");
  }

  public static void main(String[] args) {
    Wblog wblog = new Wblog();
    try {
      wblog.init();
    } catch (Exception e) {
      LOGGER.error("Could not initialize Wblog instance.", e);
    }
    wblog.run();
    wblog.closeAndSaveResources();
  }

}
