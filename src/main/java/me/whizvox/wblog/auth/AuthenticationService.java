package me.whizvox.wblog.auth;

import me.whizvox.wblog.dbo.*;
import me.whizvox.wblog.util.DateUtils;
import me.whizvox.wblog.util.StringUtils;
import org.jooq.DSLContext;

import java.nio.CharBuffer;
import java.nio.LongBuffer;
import java.util.Arrays;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static me.whizvox.wblog.Wblog.LOGGER;

public class AuthenticationService {

  public static final int
      VERIFICATION_TOKEN_LENGTH = 24,
      LOGIN_TOKEN_LENGTH = 24;

  private HashGenerator hashGen;
  private UserRepository userRepo;
  private UnverifiedUserRepository uvUserRepo;
  private LoginRepository loginRepo;

  public AuthenticationService(HashGenerator hashGen, UserRepository userRepo, UnverifiedUserRepository uvUserRepo,
                               LoginRepository loginRepo) {
    this.hashGen = hashGen;
    this.userRepo = userRepo;
    this.uvUserRepo = uvUserRepo;
    this.loginRepo = loginRepo;
  }

  private AuthenticationService() {}

  public HashGenerator getHashGenerator() {
    return hashGen;
  }

  public UserRepository getUserRepository() {
    return userRepo;
  }

  public UnverifiedUserRepository getUnverifiedUserRepository() {
    return uvUserRepo;
  }

  public LoginRepository getLoginRepository() {
    return loginRepo;
  }

  public void createRepositories() {
    userRepo.create();
    uvUserRepo.create();
    loginRepo.create();
  }

  public int getUserCount() {
    return userRepo.selectUserCount();
  }

  public enum RegisterUserResult {
    SUCCESS,
    USERNAME_TAKEN,
    EMAIL_ADDRESS_TAKEN,
    INVALID_USERNAME,
    INVALID_EMAIL_ADDRESS,
    PASSWORD_REQUIREMENTS_NOT_MET,
  }

  public RegisterUserResult registerNewUser(String username, String emailAddress, char[] password, StringBuffer tokenBuffer) throws AuthenticationException {
    User user = userRepo.selectWithUsername(username);
    if (user != null) {
      LOGGER.info("Failed to register user as their username has already been taken <username=%s, emailAddress=%s>", username, emailAddress);
      return RegisterUserResult.USERNAME_TAKEN;
    }
    user = userRepo.selectWithEmailAddress(emailAddress);
    if (user != null) {
      LOGGER.info("Failed to register user as their email address has already been taken <username=%s, emailAddress=%s>", username, emailAddress);
      return RegisterUserResult.EMAIL_ADDRESS_TAKEN;
    }
    if (password.length < 6) { // should I make this more strict? I've always hated dumb password requirements
      LOGGER.info("Failed to register user as their password failed to meet the requirements <username=%s, emailAddress=%s>", username, emailAddress);
      return RegisterUserResult.PASSWORD_REQUIREMENTS_NOT_MET;
    }
    if (!isValidUsername(username)) {
      return RegisterUserResult.INVALID_USERNAME;
    }
    if (!StringUtils.isValidEmailAddress(emailAddress)) {
      return RegisterUserResult.INVALID_EMAIL_ADDRESS;
    }
    Hash hash = hashGen.generateHash(password);
    UnverifiedUser uvUser = new UnverifiedUser();
    uvUser.username = username;
    uvUser.emailAddress = emailAddress;
    uvUser.expirationDate = DateUtils.getDaysOffsetFromNow(1);
    uvUser.password = StringUtils.bytesToHex(hash.compile());
    byte[] tokenBytes = new byte[VERIFICATION_TOKEN_LENGTH / 2];
    hashGen.randomBytes(tokenBytes);
    uvUser.token = StringUtils.bytesToHex(tokenBytes);
    Arrays.fill(tokenBytes, (byte) 0);
    uvUserRepo.insert(uvUser);
    tokenBuffer.append(uvUser.token);
    LOGGER.info("Successfully created a new unverified user <username=%s, emailAddress=%s>", username, emailAddress);
    return RegisterUserResult.SUCCESS;
  }

  public enum VerifyUserResult {
    SUCCESS,
    SUCCESS_USERNAME_TAKEN,       // should very rarely happen, but it *is* a possibility since usernames don't have to be
                          // unique for unverified users. in this case, a random username will be generated.
    EMAIL_ADDRESS_TAKEN,
    FAILURE,              // should NEVER EVER happen
    NOT_FOUND,
    EXPIRED, // rarely should happen, as expired tokens should be regularly deleted
    MALFORMED_TOKEN,
  }

  public VerifyUserResult verifyUser(String token, LongBuffer idBuffer) {
    if (token == null || token.length() != VERIFICATION_TOKEN_LENGTH) {
      LOGGER.info("Could not verify user as their token <%s> is malformed", token);
      return VerifyUserResult.MALFORMED_TOKEN;
    }
    UnverifiedUser uvUser = uvUserRepo.selectWithToken(token);
    if (uvUser == null) {
      LOGGER.info("Could not verify user as their token <%s> was not found (maybe it expired and was deleted?)", token);
      return VerifyUserResult.NOT_FOUND;
    }
    if (uvUser.expirationDate.before(new Date())) {
      LOGGER.info("Could not verify user as their token <%s> expired <%s>. Deleting it now...", token, uvUser.expirationDate);
      uvUserRepo.delete(token);
      return VerifyUserResult.EXPIRED;
    }
    // at this point, we've confirmed that the token is indeed valid, so it wouldn't be a good idea to log it
    User user = userRepo.selectWithEmailAddress(uvUser.emailAddress);
    if (user != null) {
      LOGGER.info("Could not verify user <%s> as their email address <%s> was taken", uvUser.username, uvUser.emailAddress);
      return VerifyUserResult.EMAIL_ADDRESS_TAKEN;
    }
    boolean genUsername = false;
    user = userRepo.selectWithUsername(uvUser.username);
    if (user != null) {
      LOGGER.info("Unverified user's username <%s, email=%s> has been taken. Will generate a new one...", uvUser.username, uvUser.emailAddress);
      int attempts = 0;
      genUsername = true;
      byte[] randomBytes = new byte[4];
      while (user != null && attempts < 20) {
        hashGen.randomBytes(randomBytes);
        uvUser.username = "user" + StringUtils.bytesToHex(randomBytes);
        user = userRepo.selectWithUsername(uvUser.username);
        attempts++;
      }
      if (user != null) {
        LOGGER.info("Could not generate a unique username for unverified user <%s>", uvUser.emailAddress);
        return VerifyUserResult.FAILURE;
      }
    }
    user = new User();
    user.id = -1;
    user.username = uvUser.username;
    user.emailAddress = uvUser.emailAddress;
    user.password = uvUser.password;
    user.group = "member";
    uvUserRepo.delete(token);
    userRepo.insert(user);
    idBuffer.put(user.id);
    LOGGER.info("A new user has been officially registered! <id={}, username={}, emailAddress={}>", user.id, user.username, user.emailAddress);
    if (genUsername) {
      return VerifyUserResult.SUCCESS_USERNAME_TAKEN;
    }
    return VerifyUserResult.SUCCESS;
  }

  public enum UpdateUserResult {
    SUCCESS,
    ID_NOT_FOUND,
    USERNAME_TAKEN,
    EMAIL_ADDRESS_TAKEN,
    INVALID_USERNAME,
    INVALID_EMAIL_ADDRESS,
    INVALID_PASSWORD_HASH
  }

  public UpdateUserResult updateUser(User user) {
    if (userRepo.selectWithId(user.id) == null) {
      return UpdateUserResult.ID_NOT_FOUND;
    }
    if (userRepo.selectWithUsername(user.username) != null) {
      return UpdateUserResult.USERNAME_TAKEN;
    }
    if (userRepo.selectWithEmailAddress(user.emailAddress) != null) {
      return UpdateUserResult.EMAIL_ADDRESS_TAKEN;
    }
    if (!isValidUsername(user.username)) {
      return UpdateUserResult.INVALID_USERNAME;
    }
    if (!StringUtils.isValidEmailAddress(user.emailAddress)) {
      return UpdateUserResult.INVALID_EMAIL_ADDRESS;
    }
    if ("".equals(user.name)) {
      user.name = null;
    }
    userRepo.update(user);
    return UpdateUserResult.SUCCESS;
  }

  public enum LoginResult {
    SUCCESS,
    USERNAME_NOT_FOUND,
    EMAIL_ADDRESS_NOT_FOUND,
    BAD_DATA, // god help you if this is thrown
    INCORRECT_PASSWORD
  }

  public LoginResult login(String query, CharBuffer tokenBuffer, char[] password) throws AuthenticationException {
    User user;
    boolean validEmail = StringUtils.isValidEmailAddress(query);
    if (validEmail) {
      user = userRepo.selectWithEmailAddress(query);
    } else {
      user = userRepo.selectWithUsername(query);
    }
    if (user == null) {
      if (validEmail) {
        LOGGER.info("Could not login user. Invalid email address <%s>", query);
        return LoginResult.EMAIL_ADDRESS_NOT_FOUND;
      } else {
        LOGGER.info("Could not login user. Invalid username <%s>", query);
        return LoginResult.USERNAME_NOT_FOUND;
      }
    }
    Hash hash = hashGen.parseHash(StringUtils.hexToBytes(user.password));
    if (hash == null) {
      LOGGER.error("Could not login user. Could not parse hash <%s>.", user.password);
      return LoginResult.BAD_DATA;
    }
    boolean auth = hashGen.authenticate(password, hash);
    if (!auth) {
      LOGGER.info("Could not login user <%s>. Incorrect password", query);
      return LoginResult.INCORRECT_PASSWORD;
    }
    // create a new login token for the user's browser to use
    Login login = new Login();
    login.userId = user.id;
    login.expirationDate = DateUtils.getDaysOffsetFromNow(30);
    byte[] tokenBytes = new byte[LOGIN_TOKEN_LENGTH / 2];
    hashGen.randomBytes(tokenBytes);
    login.token = StringUtils.bytesToHex(tokenBytes);
    Arrays.fill(tokenBytes, (byte) 0); // clear the bytes
    loginRepo.insert(login);
    tokenBuffer.append(login.token);

    if (validEmail) {
      LOGGER.info("User successfully logged in <emailAddress=%s>", query);
    } else {
      LOGGER.info("User successfully logged in <username=%s>", query);
    }
    return LoginResult.SUCCESS;
  }

  public enum LogoutResult {
    SUCCESS,
    TOKEN_NOT_FOUND,
    MALFORMED_TOKEN
  }

  public LogoutResult logout(String token) {
    if (token == null || token.length() != LOGIN_TOKEN_LENGTH) {
      LOGGER.info("Could not log out user as their token <%s> is malformed", token);
      return LogoutResult.MALFORMED_TOKEN;
    }
    Login login = loginRepo.selectWithToken(token);
    if (login == null) {
      LOGGER.info("Could not log out user as their token <%s> was not found in the database", token);
      return LogoutResult.TOKEN_NOT_FOUND;
    }
    loginRepo.deleteWithToken(token);
    LOGGER.info("User <id=%d> has successfully logged out", login.userId);
    return LogoutResult.SUCCESS;
  }

  public enum VerifyLoginResult {
    SUCCESS,
    TOKEN_NOT_FOUND,
    TOKEN_EXPIRED,
    MALFORMED_TOKEN
  }

  public VerifyLoginResult verifyLogin(String token, LongBuffer idBuffer) {
    if (token == null || token.length() != LOGIN_TOKEN_LENGTH) {
      LOGGER.info("Could not verify login as the token <%s> has been malformed", token);
      return VerifyLoginResult.MALFORMED_TOKEN;
    }
    Login login = loginRepo.selectWithToken(token);
    if (login == null) {
      LOGGER.info("Could not verify login as the token <%s> was not found in the database", token);
      return VerifyLoginResult.TOKEN_NOT_FOUND;
    }
    if (login.expirationDate.before(new Date())) {
      LOGGER.info("Could not verify login as the token <%s> has expired. Deleting now...", token);
      loginRepo.deleteWithToken(token);
      return VerifyLoginResult.TOKEN_EXPIRED;
    }
    idBuffer.put(login.userId);
    // don't compromise the user's login token
    LOGGER.info("User <id=%d> has successfully verified their login token", login.userId);
    return VerifyLoginResult.SUCCESS;
  }

  public static Builder newBuilder() {
    return new Builder();
  }

  public static class Builder {
    public DSLContext create;
    public HashGenerator hashGen;
    public UserRepository userRepo;
    public UnverifiedUserRepository uvUserRepo;
    public LoginRepository loginRepo;

    public Builder() {
      create = null;
      hashGen = null;
      userRepo = null;
      uvUserRepo = null;
    }

    public Builder setDslContext(DSLContext create) {
      this.create = create;
      return this;
    }

    public Builder setHashGenerator(HashGenerator hashGen) {
      this.hashGen = hashGen;
      return this;
    }

    public Builder setUserRepository(UserRepository userRepo) {
      this.userRepo = userRepo;
      return this;
    }

    public Builder setUnverifiedUserRepository(UnverifiedUserRepository uvUserRepo) {
      this.uvUserRepo = uvUserRepo;
      return this;
    }

    public Builder setLoginRepository(LoginRepository loginRepo) {
      this.loginRepo = loginRepo;
      return this;
    }

    public AuthenticationService build() {
      if (create == null) {
        if (hashGen != null && userRepo != null && uvUserRepo != null && loginRepo != null) {
          return new AuthenticationService(hashGen, userRepo, uvUserRepo, loginRepo);
        }
        return null;
      }
      if (hashGen == null) {
        hashGen = HashGenerator.builder().build();
      }
      if (userRepo == null) {
        userRepo = new UserRepository(create);
      }
      if (uvUserRepo == null) {
        uvUserRepo = new UnverifiedUserRepository(create);
      }
      if (loginRepo == null) {
        loginRepo = new LoginRepository(create);
      }
      return new AuthenticationService(hashGen, userRepo, uvUserRepo, loginRepo);
    }
  }

  private static final Pattern PATTERN_VALID_USERNAME = Pattern.compile("^[^ ][a-zA-Z0-9-_\\[\\] ]+[^ ]$");

  public static boolean isValidUsername(String username) {
    Matcher m = PATTERN_VALID_USERNAME.matcher(username);
    return m.matches();
  }

}
