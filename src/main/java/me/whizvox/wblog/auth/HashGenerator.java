package me.whizvox.wblog.auth;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.Security;
import java.security.spec.InvalidKeySpecException;
import java.util.Arrays;

public class HashGenerator {

  private final String algorithm;
  private final int iterations;
  private final int keyLength;
  private final int saltSize;

  private final SecureRandom rand;

  public HashGenerator(String algorithm, int iterations, int keyLength, int saltSize) {
    this.algorithm = algorithm;
    this.iterations = iterations;
    this.keyLength = keyLength;
    this.saltSize = saltSize;

    rand = new SecureRandom();
  }

  public void randomBytes(byte[] bytes) {
    rand.nextBytes(bytes);
  }

  private byte[] generateSalt() {
    byte[] salt = new byte[saltSize];
    randomBytes(salt);
    return salt;
  }

  private byte[] hash(char[] password, byte[] salt) throws AuthenticationException {
    PBEKeySpec spec = new PBEKeySpec(password, salt, iterations, keyLength);
    try {
      SecretKeyFactory f = SecretKeyFactory.getInstance(algorithm);
      byte[] hash = f.generateSecret(spec).getEncoded();
      Arrays.fill(password, ' ');
      spec.clearPassword();
      return hash;
    } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
      throw new AuthenticationException(e);
    }
  }

  public Hash generateHash(char[] password) throws AuthenticationException {
    byte[] salt = generateSalt();
    byte[] hashedPassword = hash(password, salt);
    Hash hash = new Hash();
    hash.salt = salt;
    hash.hashedPassword = hashedPassword;
    return hash;
  }

  public Hash parseHash(byte[] bytes) {
    Hash hash = null;
    if (bytes != null && bytes.length == keyLength / 8 + saltSize) {
      hash = new Hash();
      hash.salt = Arrays.copyOfRange(bytes, 0, saltSize);
      hash.hashedPassword = Arrays.copyOfRange(bytes, saltSize, bytes.length);
    }
    return hash;
  }

  public boolean authenticate(char[] attempt, Hash stored) throws AuthenticationException {
    if (stored != null && stored.salt != null && stored.salt.length == saltSize && stored.hashedPassword != null &&
        stored.hashedPassword.length == keyLength / 8) {
      byte[] hashedAttempt = hash(attempt, stored.salt);
      boolean res = Arrays.equals(stored.hashedPassword, hashedAttempt);
      Arrays.fill(attempt, ' ');
      return res;
    }
    return false;
  }

  public static Builder builder() {
    return new Builder();
  }

  public static class Builder {

    public static final String
        DEFAULT_ALGORITHM = "PBKDF2WITHHMACSHA256";
    public static final int
        DEFAULT_ITERATIONS = 10000,
        DEFAULT_KEY_LENGTH = 256,
        DEFAULT_SALT_SIZE = 8;

    public String algorithm;
    public int iterations;
    public int keyLength;
    public int saltSize;

    public Builder() {
      setDefaultAlgorithm();
      setDefaultIterations();
      setDefaultKeyLength();
      setDefaultSaltSize();
    }

    public Builder setAlgorithm(String algorithm) {
      this.algorithm = algorithm;
      return this;
    }
    public Builder setDefaultAlgorithm() {
      return setAlgorithm(DEFAULT_ALGORITHM);
    }
    public Builder setIterations(int iterations) {
      this.iterations = iterations;
      return this;
    }
    public Builder setDefaultIterations() {
      return setIterations(DEFAULT_ITERATIONS);
    }
    public Builder setKeyLength(int keyLength) {
      this.keyLength = keyLength;
      return this;
    }
    public Builder setDefaultKeyLength() {
      return setKeyLength(DEFAULT_KEY_LENGTH);
    }
    public Builder setSaltSize(int saltSize) {
      this.saltSize = saltSize;
      return this;
    }
    public Builder setDefaultSaltSize() {
      return setSaltSize(DEFAULT_SALT_SIZE);
    }

    public HashGenerator build() {
      boolean algoFound = false;
      for (String a : Security.getAlgorithms("SecretKeyFactory")) {
        if (a.equalsIgnoreCase(algorithm)) {
          algoFound = true;
          break;
        }
      }
      if (!algoFound ||
          (iterations < 0 || iterations > 10000000) ||
          (keyLength < 64 || keyLength > 8192) ||
          (saltSize < 4 || saltSize > 32)) {
        return null;
      }
      return new HashGenerator(algorithm, iterations, keyLength, saltSize);
    }
  }

}
