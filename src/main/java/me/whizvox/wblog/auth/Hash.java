package me.whizvox.wblog.auth;

import me.whizvox.wblog.util.StringUtils;

public class Hash {
  public byte[] salt;
  public byte[] hashedPassword;

  public byte[] compile() {
    byte[] r = new byte[salt.length + hashedPassword.length];
    System.arraycopy(salt, 0, r, 0, salt.length);
    System.arraycopy(hashedPassword, 0, r, salt.length, hashedPassword.length);
    return r;
  }

  // not meant to be used as the thing that actually is stored in the database, but in this project, it happens to
  // be the same thing
  @Override
  public String toString() {
    return StringUtils.bytesToHex(compile());
  }
}
