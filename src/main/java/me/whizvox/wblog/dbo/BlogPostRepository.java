package me.whizvox.wblog.dbo;

import me.whizvox.wblog.generated.tables.records.BlogPostsRecord;
import me.whizvox.wblog.util.JooqRepository;
import me.whizvox.wblog.util.RecordParser;
import org.jooq.DSLContext;

import static me.whizvox.wblog.generated.tables.BlogPosts.BLOG_POSTS;

public class BlogPostRepository extends JooqRepository {

  public BlogPostRepository(DSLContext create) {
    super(create, BLOG_POSTS);
  }

  public boolean insert(BlogPost post) {
    return create.insertInto(BLOG_POSTS)
        .set(PARSER.fromObject(post))
        .execute() > 0;
  }

  public boolean update(BlogPost post) {
    return create.update(BLOG_POSTS)
        .set(PARSER.fromObject(post))
        .execute() > 0;
  }

  public boolean delete(long id) {
    return create.deleteFrom(BLOG_POSTS)
        .where(BLOG_POSTS.ID.eq(id))
        .execute() > 0;
  }

  public BlogPost selectWithId(long id) {
    BlogPostsRecord r = create.selectFrom(BLOG_POSTS)
        .where(BLOG_POSTS.ID.eq(id))
        .fetchAny();
    return PARSER.fromRecord(r);
  }

  private static final RecordParser<BlogPost, BlogPostsRecord> PARSER = new RecordParser<BlogPost, BlogPostsRecord>() {
    @Override
    public BlogPost fromRecord(BlogPostsRecord record) {
      if (record == null) {
        return null;
      }
      BlogPost post = new BlogPost();
      post.id = record.getId();
      post.authorId = record.getAuthorId();
      post.commentsDisabled = record.getCommentsDisabled();
      return post;
    }

    @Override
    public BlogPostsRecord fromObject(BlogPost post) {
      if (post == null) {
        return null;
      }
      BlogPostsRecord record = new BlogPostsRecord();
      record.setId(post.id);
      record.setAuthorId(post.authorId);
      record.setCommentsDisabled(post.commentsDisabled);
      return record;
    }
  };

}
