package me.whizvox.wblog.dbo;

import java.util.Date;

/**
 * Basically the same as a {@link User}, but does not require a unique username or password, nor does it not yet have
 * an assigned numerical id. Can only be accessed via a string-based token sent to the user's email address, and then
 * given a status as an actual User.
 */
public class UnverifiedUser {

  public String token;
  public Date expirationDate;
  public String username;
  public String emailAddress;
  public String password;

}
