package me.whizvox.wblog.dbo;

import me.whizvox.wblog.generated.tables.records.UsersRecord;
import me.whizvox.wblog.util.GenIdJooqRepository;
import me.whizvox.wblog.util.RecordParser;
import me.whizvox.wblog.util.SqlUtils;
import org.jooq.DSLContext;
import org.jooq.Result;

import java.util.List;

import static me.whizvox.wblog.generated.tables.Users.USERS;

public class UserRepository extends GenIdJooqRepository {

  private DSLContext create;

  public UserRepository(DSLContext create) {
    super(create, USERS);
    this.create = create;
  }

  @Override
  public void create() {
    super.create();
    UsersRecord r = create.selectFrom(USERS)
        .orderBy(USERS.ID.desc())
        .limit(1)
        .fetchAny();
    if (r != null) {
      setNextAvailableId(r.getId());
      increaseNextAvailableId();
    }
  }

  public void insert(User user) {
    boolean generateId = user.id < 1;
    if (generateId) {
      user.id = getNextAvailableId();
    }
    create.insertInto(USERS)
        .set(PARSER.fromObject(user))
        .execute();
    if (generateId) {
      increaseNextAvailableId();
    }
  }

  public boolean delete(long id) {
    return create.deleteFrom(USERS)
        .where(USERS.ID.eq(id))
        .execute() > 0;
  }

  public boolean update(User user) {
    return create.update(USERS)
        .set(PARSER.fromObject(user))
        .where(USERS.ID.eq(user.id))
        .execute() > 0;
  }

  public User selectWithId(long id) {
    UsersRecord r = create.selectFrom(USERS)
        .where(USERS.ID.eq(id))
        .fetchAny();
    return PARSER.fromRecord(r);
  }

  public User selectWithSearchEquals(String query) {
    UsersRecord r = create.selectFrom(USERS)
        .where(USERS.USERNAME.lower().eq(query.toLowerCase()))
        .or(USERS.EMAIL_ADDRESS.lower().eq(query.toLowerCase()))
        .fetchAny();
    return PARSER.fromRecord(r);
  }

  public List<User> selectWithSearchLike(String query) {
    Result<UsersRecord> r = create.selectFrom(USERS)
        .where(USERS.USERNAME.like("%" + query + "%"))
        .or(USERS.EMAIL_ADDRESS.like("%" + query + "%"))
        .fetch();
    return PARSER.fromRecords(r);
  }

  public User selectWithUsername(String username) {
    UsersRecord r = create.selectFrom(USERS)
        .where(USERS.USERNAME.like(username))
        .fetchAny();
    return PARSER.fromRecord(r);
  }

  public User selectWithEmailAddress(String emailAddress) {
    UsersRecord r = create.selectFrom(USERS)
        .where(USERS.EMAIL_ADDRESS.like(emailAddress))
        .fetchAny();
    return PARSER.fromRecord(r);
  }

  public int selectUserCount() {
    return create.selectCount()
        .fetch()
        .get(1)
        .value1();
  }

  private static final RecordParser<User, UsersRecord> PARSER = new RecordParser<User, UsersRecord>() {
    @Override
    public User fromRecord(UsersRecord record) {
      if (record != null) {
        User user = new User();
        user.id = record.getId();
        user.username = record.getUsername();
        user.emailAddress = record.getEmailAddress();
        user.password = record.getPassword();
        user.group = record.getGroupName();
        user.name = record.getName();
        user.biography = record.getBiography();
        user.lastLogin = record.getLastLogin();
        return user;
      }
      return null;
    }
    @Override
    public UsersRecord fromObject(User user) {
      if (user != null) {
        UsersRecord record = new UsersRecord();
        record.setId(user.id);
        record.setUsername(user.username);
        record.setEmailAddress(user.emailAddress);
        record.setPassword(user.password);
        record.setGroupName(user.group);
        record.setName(user.name);
        record.setBiography(user.biography);
        record.setLastLogin(SqlUtils.getTimestampFromDate(user.lastLogin));
        return record;
      }
      return null;
    }
  };

}
