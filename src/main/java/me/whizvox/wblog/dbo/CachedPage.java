package me.whizvox.wblog.dbo;

import me.whizvox.wblog.util.IntIdentifiable;

import java.util.Date;

public class CachedPage extends IntIdentifiable {

  public String path;
  public String language;
  public String title;
  public String contents;
  public Date lastUpdated;

}
