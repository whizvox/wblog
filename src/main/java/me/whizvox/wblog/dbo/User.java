package me.whizvox.wblog.dbo;

import me.whizvox.wblog.util.IntIdentifiable;

import java.util.Date;

public class User extends IntIdentifiable {

  public String username;
  public String emailAddress;
  // TODO: Add two-factor authentication stuff
  public String password;
  public String group;
  public String name;
  public String biography;
  public Date lastLogin;

}
