package me.whizvox.wblog.dbo;

public class BlogPost extends Page {

  public long authorId;
  public boolean commentsDisabled;

  public static BlogPost fromPage(Page base, BlogPost other) {
    if (base == null || other == null) {
      return null;
    }
    BlogPost post = new BlogPost();
    post.path = base.path;
    post.language = base.language;
    post.title = base.title;
    post.template = base.template;
    post.contents = base.contents;
    post.syntax = base.syntax;
    post.tags = base.tags;
    post.published = base.published;
    post.lastEdited = base.lastEdited;
    post.authorId = other.authorId;
    post.commentsDisabled = other.commentsDisabled;
    return post;
  }

}
