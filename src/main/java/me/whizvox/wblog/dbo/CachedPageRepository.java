package me.whizvox.wblog.dbo;

import me.whizvox.wblog.generated.tables.records.CachedPagesRecord;
import me.whizvox.wblog.util.JooqRepository;
import me.whizvox.wblog.util.RecordParser;
import me.whizvox.wblog.util.SqlUtils;
import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.Result;

import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import static me.whizvox.wblog.generated.tables.CachedPages.CACHED_PAGES;

public class CachedPageRepository extends JooqRepository {

  private AtomicLong idGenerator;

  public CachedPageRepository(DSLContext create) {
    super(create, CACHED_PAGES);
    idGenerator = new AtomicLong(0);
  }

  @Override
  public void create() {
    super.create();
    CachedPagesRecord r = create.selectFrom(CACHED_PAGES)
        .orderBy(CACHED_PAGES.ID.desc())
        .limit(1)
        .fetchAny();
    if (r != null) {
      idGenerator.set(r.getId());
    }
  }

  public boolean insert(CachedPage cachedPage) {
    if (cachedPage.id < 1) {
      cachedPage.id = idGenerator.get() + 1;
    }
    boolean result = create.insertInto(CACHED_PAGES)
        .set(PARSER.fromObject(cachedPage))
        .execute() > 0;
    if (result) {
      idGenerator.set(cachedPage.id);
    }
    return result;
  }

  public boolean update(CachedPage cachedPage) {
    return create.update(CACHED_PAGES)
        .set(PARSER.fromObject(cachedPage))
        .where(CACHED_PAGES.ID.eq(cachedPage.id))
        .execute() > 0;
  }

  public boolean delete(long id) {
    return create.deleteFrom(CACHED_PAGES)
        .where(CACHED_PAGES.ID.eq(id))
        .execute() > 0;
  }

  public boolean deleteAll() {
    return create.deleteFrom(CACHED_PAGES)
        .execute() > 0;
  }

  public CachedPage selectWithId(long id) {
    CachedPagesRecord r = create.selectFrom(CACHED_PAGES)
        .where(CACHED_PAGES.ID.eq(id))
        .fetchAny();
    return PARSER.fromRecord(r);
  }

  public CachedPage selectWithPathAndLanguage(String path, String language) {
    CachedPagesRecord r = create.selectFrom(CACHED_PAGES)
        .where(CACHED_PAGES.PATH.eq(path))
        .and(CACHED_PAGES.LANGUAGE.eq(path))
        .fetchAny();
    return PARSER.fromRecord(r);
  }

  public List<CachedPage> selectWithPath(String path) {
    Result<CachedPagesRecord> r = create.selectFrom(CACHED_PAGES)
        .where(CACHED_PAGES.PATH.eq(path))
        .fetch();
    return PARSER.fromRecords(r);
  }

  public CachedPage selectWithPathAndLanguageNoContents(String path, String language) {
      Record r = create.select(CACHED_PAGES.ID, CACHED_PAGES.PATH, CACHED_PAGES.LANGUAGE, CACHED_PAGES.TITLE, CACHED_PAGES.LAST_UPDATED)
          .from(CACHED_PAGES)
          .where(CACHED_PAGES.PATH.eq(path))
          .and(CACHED_PAGES.LANGUAGE.eq(language))
          .fetchAny();
      if (r == null) {
        return null;
      }
      CachedPage page = new CachedPage();
      page.id = r.get(CACHED_PAGES.ID);
      page.path = r.get(CACHED_PAGES.PATH);
      page.language = r.get(CACHED_PAGES.LANGUAGE);
      page.title = r.get(CACHED_PAGES.TITLE);
      page.lastUpdated = r.get(CACHED_PAGES.LAST_UPDATED);
      return page;
  }

  private static final RecordParser<CachedPage, CachedPagesRecord> PARSER = new RecordParser<CachedPage, CachedPagesRecord>() {
    @Override
    public CachedPage fromRecord(CachedPagesRecord record) {
      if (record == null) {
        return null;
      }
      CachedPage page = new CachedPage();
      page.id = record.getId();
      page.path = record.getPath();
      page.language = record.getLanguage();
      page.title = record.getTitle();
      page.contents = record.getContents();
      page.lastUpdated = record.getLastUpdated();
      return page;
    }

    @Override
    public CachedPagesRecord fromObject(CachedPage cachedPage) {
      if (cachedPage == null) {
        return null;
      }
      CachedPagesRecord record = new CachedPagesRecord();
      record.setId(cachedPage.id);
      record.setPath(cachedPage.path);
      record.setLanguage(cachedPage.language);
      record.setTitle(cachedPage.title);
      record.setContents(cachedPage.contents);
      record.setLastUpdated(SqlUtils.getTimestampFromDate(cachedPage.lastUpdated));
      return record;
    }
  };

}
