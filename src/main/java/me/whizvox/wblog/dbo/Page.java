package me.whizvox.wblog.dbo;

import lombok.Getter;
import lombok.Setter;
import me.whizvox.wblog.util.IntIdentifiable;

import java.util.Date;

public class Page extends IntIdentifiable {

  @Getter
  public String path;

  @Getter
  public String language;

  @Getter
  public String template;

  @Getter
  public String title;

  @Getter
  public String contents;

  @Getter
  public String syntax;

  @Getter
  public String[] tags;

  @Getter
  public Date published;

  @Getter
  public Date lastEdited;

}
