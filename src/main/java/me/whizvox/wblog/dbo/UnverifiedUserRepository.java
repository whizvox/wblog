package me.whizvox.wblog.dbo;

import me.whizvox.wblog.generated.tables.records.UnverifiedUsersRecord;
import me.whizvox.wblog.util.JooqRepository;
import me.whizvox.wblog.util.RecordParser;
import me.whizvox.wblog.util.SqlUtils;
import org.jooq.DSLContext;
import org.jooq.Result;

import java.util.List;

import static me.whizvox.wblog.generated.tables.UnverifiedUsers.UNVERIFIED_USERS;

public class UnverifiedUserRepository extends JooqRepository {

  public UnverifiedUserRepository(DSLContext create) {
    super(create, UNVERIFIED_USERS);
  }

  public boolean insert(UnverifiedUser nvUser) {
    return create.insertInto(UNVERIFIED_USERS)
        .set(PARSER.fromObject(nvUser))
        .execute() > 0;
  }

  public boolean delete(String token) {
    return create.deleteFrom(UNVERIFIED_USERS)
        .where(UNVERIFIED_USERS.TOKEN.eq(token))
        .execute() > 0;
  }

  public UnverifiedUser selectWithToken(String token) {
    UnverifiedUsersRecord r = create.selectFrom(UNVERIFIED_USERS)
        .where(UNVERIFIED_USERS.TOKEN.eq(token))
        .fetchAny();
    return PARSER.fromRecord(r);
  }

  public List<UnverifiedUser> selectExpiredUsers() {
    Result<UnverifiedUsersRecord> r = create.selectFrom(UNVERIFIED_USERS)
        .where(UNVERIFIED_USERS.EXPIRATION_DATE.lessThan(SqlUtils.getCurrentTimestamp()))
        .fetch();
    return PARSER.fromRecords(r);
  }

  public int deleteExpiredUsers() {
    return create.deleteFrom(UNVERIFIED_USERS)
        .where(UNVERIFIED_USERS.EXPIRATION_DATE.lessThan(SqlUtils.getCurrentTimestamp()))
        .execute();
  }

  private static final RecordParser<UnverifiedUser, UnverifiedUsersRecord> PARSER =
      new RecordParser<UnverifiedUser, UnverifiedUsersRecord>() {
    @Override
    public UnverifiedUser fromRecord(UnverifiedUsersRecord record) {
      if (record != null) {
        UnverifiedUser uvUser = new UnverifiedUser();
        uvUser.token = record.getToken();
        uvUser.expirationDate = record.getExpirationDate();
        uvUser.username = record.getUsername();
        uvUser.emailAddress = record.getEmailAddress();
        uvUser.password = record.getPassword();
        return uvUser;
      }
      return null;
    }

    @Override
    public UnverifiedUsersRecord fromObject(UnverifiedUser uvUser) {
      if (uvUser != null) {
        UnverifiedUsersRecord record = new UnverifiedUsersRecord();
        record.setToken(uvUser.token);
        record.setExpirationDate(SqlUtils.getTimestampFromDate(uvUser.expirationDate));
        record.setUsername(uvUser.username);
        record.setEmailAddress(uvUser.emailAddress);
        record.setPassword(uvUser.password);
        return record;
      }
      return null;
    }
  };

}
