package me.whizvox.wblog.dbo;

import me.whizvox.wblog.generated.tables.records.LoginsRecord;
import me.whizvox.wblog.util.JooqRepository;
import me.whizvox.wblog.util.RecordParser;
import me.whizvox.wblog.util.SqlUtils;
import org.jooq.DSLContext;
import org.jooq.Result;

import java.util.List;

import static me.whizvox.wblog.generated.tables.Logins.LOGINS;

public class LoginRepository extends JooqRepository {

  public LoginRepository(DSLContext create) {
    super(create, LOGINS);
  }

  public boolean insert(Login login) {
    return create.insertInto(LOGINS)
        .set(PARSER.fromObject(login))
        .execute() > 0;
  }

  public boolean deleteWithToken(String token) {
    return create.deleteFrom(LOGINS)
        .where(LOGINS.TOKEN.eq(token))
        .execute() > 0;
  }

  public boolean deleteWithUserId(long userId) {
    return create.deleteFrom(LOGINS)
        .where(LOGINS.USER_ID.eq(userId))
        .execute() > 0;
  }

  public int deleteExpiredLogins() {
    return create.deleteFrom(LOGINS)
        .where(LOGINS.EXPIRATION_DATE.lessThan(SqlUtils.getCurrentTimestamp()))
        .execute();
  }

  public Login selectWithToken(String token) {
    LoginsRecord r = create.selectFrom(LOGINS)
        .where(LOGINS.TOKEN.eq(token))
        .fetchAny();
    return PARSER.fromRecord(r);
  }

  public List<Login> selectWithUserId(long userId) {
    Result<LoginsRecord> r = create.selectFrom(LOGINS)
        .where(LOGINS.USER_ID.eq(userId))
        .fetch();
    return PARSER.fromRecords(r);
  }

  public List<Login> selectExpiredLogins() {
    Result<LoginsRecord> r = create.selectFrom(LOGINS)
        .where(LOGINS.EXPIRATION_DATE.lessThan(SqlUtils.getCurrentTimestamp()))
        .fetch();
    return PARSER.fromRecords(r);
  }

  private static final RecordParser<Login, LoginsRecord> PARSER = new RecordParser<Login, LoginsRecord>() {
    @Override
    public Login fromRecord(LoginsRecord record) {
      if (record == null) {
        return null;
      }
      Login login = new Login();
      login.token = record.getToken();
      login.userId = record.getUserId();
      login.expirationDate = record.getExpirationDate();
      return login;
    }

    @Override
    public LoginsRecord fromObject(Login login) {
      if (login == null) {
        return null;
      }
      LoginsRecord record = new LoginsRecord();
      record.setToken(login.token);
      record.setUserId(login.userId);
      record.setExpirationDate(SqlUtils.getTimestampFromDate(login.expirationDate));
      return record;
    }
  };

}
