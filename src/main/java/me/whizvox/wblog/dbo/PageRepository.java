package me.whizvox.wblog.dbo;

import me.whizvox.wblog.generated.tables.records.PagesRecord;
import me.whizvox.wblog.util.JooqRepository;
import me.whizvox.wblog.util.RecordParser;
import me.whizvox.wblog.util.SqlUtils;
import me.whizvox.wblog.util.StringUtils;
import org.jooq.*;

import java.sql.Timestamp;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import static me.whizvox.wblog.generated.tables.Pages.PAGES;

public class PageRepository extends JooqRepository {

  private AtomicLong idGenerator;

  public PageRepository(DSLContext create) {
    super(create, PAGES);
    idGenerator = new AtomicLong(0);
  }

  private SelectJoinStep<Record9<Long, String, String, String, String, String, String, Timestamp, Timestamp>> setupNoContents() {
    return create.select(PAGES.ID, PAGES.PATH, PAGES.LANGUAGE, PAGES.TEMPLATE, PAGES.TITLE, PAGES.SYNTAX, PAGES.TAGS, PAGES.PUBLISHED, PAGES.LAST_EDITED)
        .from(PAGES);
  }

  @Override
  public void create() {
    super.create();
    PagesRecord r = create.selectFrom(PAGES)
        .orderBy(PAGES.ID.desc())
        .limit(1)
        .fetchAny();
    if (r != null) {
      idGenerator.set(r.getId());
    }
  }

  public boolean insert(Page page) {
    if (page.id < 1) {
      // don't increase it just yet, in case the insert is not successful and throws an exception
      page.id = idGenerator.get() + 1;
    }
    boolean result = create.insertInto(PAGES)
        .set(PARSER.fromObject(page))
        .execute() > 0;
    if (result) {
      idGenerator.set(page.id);
    }
    return result;
  }

  public boolean update(Page page) {
    return create.update(PAGES)
        .set(PARSER.fromObject(page))
        .where(PAGES.ID.eq(page.id))
        .execute() > 0;
  }

  public boolean delete(long id) {
    return create.deleteFrom(PAGES)
        .where(PAGES.ID.eq(id))
        .execute() > 0;
  }

  public Page selectWithId(long id) {
    PagesRecord r = create.selectFrom(PAGES)
        .where(PAGES.ID.eq(id))
        .fetchAny();
    return PARSER.fromRecord(r);
  }

  public Page selectWithPathAndLanguage(String path, String language) {
    PagesRecord r = create.selectFrom(PAGES)
        .where(PAGES.PATH.eq(path))
        .and(PAGES.LANGUAGE.eq(language))
        .fetchAny();
    return PARSER.fromRecord(r);
  }

  public List<Page> selectWithPath(String path) {
    Result<PagesRecord> r = create.selectFrom(PAGES)
        .where(PAGES.PATH.eq(path))
        .fetch();
    return PARSER.fromRecords(r);
  }

  @SuppressWarnings("unchecked")
  public List<Page> searchGeneral(String query, int maxQueryItems) {
    List<String> queryItems = StringUtils.breakupArgumentTokens(query, maxQueryItems);
    if (queryItems.isEmpty()) {
      return null;
    }
    String conditional = SqlUtils.prepareSearchStatement(queryItems.size(),
        PAGES.TITLE.getName(),
        PAGES.CONTENTS.getName(),
        PAGES.TAGS.getName()
        );
    queryItems = SqlUtils.prepareLikeQueryItems(queryItems, 3);
    Result r = setupNoContents()
        .where(conditional, queryItems)
        .fetch();
    return PARSER_NO_CONTENTS.fromRecords(r);
  }

  private static final RecordParser<Page, PagesRecord> PARSER = new RecordParser<Page, PagesRecord>() {
    @Override
    public Page fromRecord(PagesRecord record) {
      if (record == null) {
        return null;
      }
      Page page = new Page();
      page.id = record.getId();
      page.path = record.getPath();
      page.language = record.getLanguage();
      page.template = record.getTemplate();
      page.title = record.getTitle();
      page.contents = record.getContents();
      page.syntax = record.getSyntax();
      page.tags = StringUtils.arrayFromList(StringUtils.tokenizeTags(record.getTags(), false));
      page.published = record.getPublished();
      page.lastEdited = record.getLastEdited();
      return page;
    }

    @Override
    public PagesRecord fromObject(Page page) {
      if (page == null) {
        return null;
      }
      PagesRecord record = new PagesRecord();
      record.setId(page.id);
      record.setPath(page.path);
      record.setLanguage(page.language);
      record.setTemplate(page.template);
      record.setTitle(page.title);
      record.setContents(page.contents);
      record.setSyntax(page.syntax);
      record.setTags(StringUtils.tagsToString(page.tags));
      record.setPublished(SqlUtils.getTimestampFromDate(page.published));
      record.setLastEdited(SqlUtils.getTimestampFromDate(page.lastEdited));
      return record;
    }
  };

  private static final RecordParser<Page, Record> PARSER_NO_CONTENTS = new RecordParser<Page, Record>() {
    @Override
    public Page fromRecord(Record record) {
      if (record == null) {
        return null;
      }
      Page page = new Page();
      page.id = record.get(PAGES.ID);
      page.path = record.get(PAGES.PATH);
      page.language = record.get(PAGES.LANGUAGE);
      page.template = record.get(PAGES.TEMPLATE);
      page.title = record.get(PAGES.TITLE);
      page.syntax = record.get(PAGES.SYNTAX);
      page.tags = StringUtils.arrayFromList(StringUtils.tokenizeTags(record.get(PAGES.TAGS), false));
      page.published = record.get(PAGES.PUBLISHED);
      page.lastEdited = record.get(PAGES.LAST_EDITED);
      return page;
    }

    @Override
    public Record fromObject(Page object) {
      return null; // should not be used
    }
  };

}
