package me.whizvox.wblog.event;

import static me.whizvox.wblog.Wblog.LOGGER;

public class ServerEventHandler implements EventHandler<ServerEvent> {

  private ServerEventHandler() {}

  @Override
  public void handle(ServerEvent event) {
    // TODO: Check if user from userId is allowed to shutdown the server
    switch (event.type) {
      case ServerEvent.TYPE_START: {
        // TODO: Schedule start
        break;
      }
      case ServerEvent.TYPE_SHUTDOWN: {
        event.wblog.scheduleShutdown(event.seconds);
        break;
      }
      case ServerEvent.TYPE_RESTART: {
        event.wblog.scheduleRestart(event.seconds);
        break;
      }
      default: {
        LOGGER.error("An illegal server event was posted <userId=%d, seconds=%d, type=%s>", event.userId, event.seconds, event.type);
      }
    }
  }

  public static final ServerEventHandler INSTANCE = new ServerEventHandler();

}
