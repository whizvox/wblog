package me.whizvox.wblog.event;

import me.whizvox.wblog.Wblog;

public class ServerEvent extends WblogEvent {

  public int userId;
  public int seconds;
  public int type;

  public ServerEvent(Wblog wblog) {
    super(wblog);
  }

  public static final int
      TYPE_START = 0,
      TYPE_SHUTDOWN = 1,
      TYPE_RESTART = 2;

}
