package me.whizvox.wblog.event;

import me.whizvox.wblog.Wblog;

public class WblogEvent implements Event {

  public Wblog wblog;

  public WblogEvent(Wblog wblog) {
    this.wblog = wblog;
  }

}
