package me.whizvox.wblog.event;

public interface EventHandler<EVENT extends Event> {

  void handle(EVENT event);

}
