package me.whizvox.wblog.event;

import me.whizvox.wblog.util.MultiMap;

import java.util.List;
import java.util.Map;

public class EventManager {

  private MultiMap<Class, Event> events;
  private MultiMap<Class, EventHandler> handlers;

  public EventManager() {
    events = new MultiMap<>();
    handlers = new MultiMap<>();
  }

  public <EVENT extends Event> void registerHandler(EventHandler<EVENT> handler, Class<EVENT> cls) {
    handlers.add(cls, handler);
  }

  public synchronized <EVENT extends Event> void postEvent(EVENT event, Class<EVENT> cls) {
    events.add(cls, event);
  }

  public void postEvent(Event event) {
    events.add(event.getClass(), event);
  }

  public void loop() {
    if (!events.isEmpty()) {
      for (Map.Entry<Class, List<Event>> entry : events.entrySet()) {
        List<Event> events = entry.getValue();
        if (events != null && !events.isEmpty()) {
          List<EventHandler> h = handlers.get(entry.getKey());
          if (h != null && !h.isEmpty()) {
            for (Event e : events) {
              h.forEach(handler -> handler.handle(e));
            }
          }
        }
      }
      events.clear();
    }
  }

}
