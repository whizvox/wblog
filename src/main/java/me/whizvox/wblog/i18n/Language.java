package me.whizvox.wblog.i18n;

import me.whizvox.wblog.util.StringUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Language {

  private Map<String, String> phrases;

  public Language(Map<String, String> phrases) {
    this.phrases = phrases;
  }

  public boolean hasPhrase(String key) {
    return phrases.containsKey(key);
  }

  public String getPhrase(String key) {
    return phrases.getOrDefault(key, null);
  }

  public static Language parse(BufferedReader reader) throws IOException {
    Map<String, String> phrases = new HashMap<>();
    List<String> tokens = new ArrayList<>();
    String line;
    while ((line = reader.readLine()) != null) {
      line = line.trim();
      if (!line.isEmpty() && line.charAt(0) != '#') {
        StringUtils.tokenize(line, '=', tokens, 2);
        if (tokens.size() == 2) {
          String key = tokens.get(0).trim();
          String phrase = tokens.get(1).trim();
          phrases.put(key, phrase);
        }
        tokens.clear();
      }
    }
    return new Language(phrases);
  }

  public static Language parse(URL url) throws IOException {
    try (BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream(), StandardCharsets.UTF_8))) {
      return parse(reader);
    }
  }

}
