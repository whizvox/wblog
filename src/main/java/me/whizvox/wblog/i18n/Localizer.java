package me.whizvox.wblog.i18n;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import freemarker.core.Environment;
import freemarker.ext.beans.BeanModel;
import freemarker.template.TemplateHashModel;
import freemarker.template.TemplateMethodModelEx;
import freemarker.template.TemplateModel;
import freemarker.template.TemplateModelException;
import me.whizvox.wblog.Reference;
import me.whizvox.wblog.Wblog;
import me.whizvox.wblog.dbo.Page;

import java.io.IOException;
import java.nio.file.Path;
import java.util.*;

import static me.whizvox.wblog.Wblog.LOGGER;

public class Localizer {

  private Map<String, Language> languages;
  private String fallbackKey;
  private Language fallback;

  public Localizer(String fallback) {
    languages = new HashMap<>();
    this.fallbackKey = fallback;
    this.fallback = null;
  }

  public void registerNewLanguage(String key, Language lang) {
    languages.put(key, lang);
    if (key.equals(fallbackKey)) {
      fallback = lang;
    }
  }

  public Set<String> getLanguages() {
    return languages.keySet();
  }

  public boolean hasFallback() {
    return fallback != null;
  }

  public String getFallbackLanguage() {
    return fallbackKey;
  }

  public boolean hasLanguage(String key) {
    return languages.containsKey(key);
  }

  private Language getLanguage(String key) {
    Language lang = languages.getOrDefault(key, null);
    if (lang == null && hasFallback()) {
      lang = fallback;
    }
    return lang;
  }

  public boolean hasPhrase(String langKey, String phraseKey) {
    Language lang = getLanguage(langKey);
    if (lang == null) {
      return false;
    }
    return lang.hasPhrase(phraseKey);
  }

  public String getPhrase(String langKey, String phraseKey) {
    Language lang = getLanguage(langKey);
    if (lang == null) {
      return phraseKey;
    }
    String phrase = lang.getPhrase(phraseKey);
    if (phrase == null) {
      return phraseKey;
    }
    return phrase;
  }

  public String getFormattedPhrase(String langKey, String phraseKey, Object... args) {
    return String.format(getPhrase(langKey, phraseKey), args);
  }

  public String getLocalizedName(String langKey) {
    return getPhrase(langKey, Reference.LanguageKeys.NAME);
  }

  public static class LocalizeFreemarkerMethod implements TemplateMethodModelEx {
    private Localizer localizer;

    public LocalizeFreemarkerMethod(Localizer localizer) {
      this.localizer = localizer;
    }

    @Override
    public Object exec(List arguments) throws TemplateModelException {
        if (arguments.size() < 1) {
        throw new TemplateModelException("Must contain at least 1 argument.");
      }
      String[] args = new String[arguments.size()];
      for (int i = 0; i < args.length; i++) {
        args[i] = arguments.get(i).toString();
      }
      TemplateHashModel dm = Environment.getCurrentEnvironment().getDataModel();
      TemplateModel pageModel = dm.get("htmlPage");
      String langKey = localizer.getFallbackLanguage();
      if (pageModel instanceof BeanModel) {
        try {
          Page page = (Page) ((BeanModel) pageModel).getWrappedObject();
          langKey = page.language;
        } catch (ClassCastException ignored) {}
      }
      if (args.length == 1) {
        return localizer.getPhrase(langKey, args[0]);
      }
      return localizer.getFormattedPhrase(langKey, args[0], (Object[]) Arrays.copyOfRange(args, 1, args.length));
    }

  }

}
