# The Test Suite

This module is configured to be able to run a test suite
dependant on the core Wblog project. Any changes made to
`src/main/java/TestSuite.java` should not matter to the project
as a whole.