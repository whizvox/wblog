import me.whizvox.wblog.Wblog;

public class TestSuite {

  public static void main(String[] args) throws Exception {
    try (Wblog wblog = new Wblog("rundir")) {
      wblog.init();
      wblog.run();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

}
