# The jOOQ Generator

This is how I generate jOOQ-related files for this project.
You first need SQLite 3 installed to modify `library.db`. You
shouldn't need to change anything else for the time being.
Once you've added/modified whatever tables/schemas/etc you
needed, run the `generate` script, and jOOQ will do the hard
work for you.